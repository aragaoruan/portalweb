const configs = angular.module("app.configs", []).constant("CONFIG", {

  // urlG2: 'http://dev1g2.ead1.net/',
  // urlApi: 'http://dev1g2.ead1.net/',

  // urlG2: 'http://g2manutencao.unyleya.xyz/',
  // urlApi: 'http://g2manutencao.unyleya.xyz/api-v2',

  // urlG2: 'http://g2release.unyleya.xyz/',
  // urlApi: 'http://g2release.unyleya.xyz/api-v2',

  urlG2: "http://g2s.unyleya.com.br/",
  urlApi: "http://g2s.unyleya.com.br/api-v2",

  tokenApi: "ceb67e61be84d393a46495e36876bcd8d49e7e545a28de8a",
  tokenTypeAlg: "HS512",
  idEntidade: 1035,
  analytics: "UA-79584946-34"
});

export default configs;
