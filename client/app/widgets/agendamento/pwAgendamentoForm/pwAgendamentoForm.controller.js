import loadingGif from '../../../../assets/img/loading.gif';
import templateEventsCalendar from './eventosTemplate.html';

class PwAgendamentoFormController {

  constructor(CONFIG, $q, NotifyService, PessoaService, AgendamentoService, EnderecoService, TipoEnderecoConstante, SituacaoConstante, PaisConstante, $state, $localStorage, calendarConfig, $templateCache, $window) {

    'ngInject';

    this.name = 'pwAgendamentoForm';

    this.CONFIG = CONFIG;
    this.$q = $q;
    this.NotifyService = NotifyService;
    this.PessoaService = PessoaService;
    this.AgendamentoService = AgendamentoService;
    this.EnderecoService = EnderecoService;
    this.TipoEnderecoConstante = TipoEnderecoConstante;
    this.SituacaoConstante = SituacaoConstante;
    this.PaisConstante = PaisConstante;
    this.$state = $state;
    this.$localStorage = $localStorage;
    this.$window = $window;

    this.loadingGif = loadingGif;
    this.idMatricula = $state.params.idMatriculaAtiva;

    this.etapas = ['endereco', 'calendario'];
    this.etapaAtual = -1;

    this.endereco = {id_tipoendereco: this.TipoEnderecoConstante.CORRESPONDENCIA, bl_padrao: 1};
    this.agendamento = {id_matricula: this.idMatricula};
    this.ultimoAgendamento = {};

    this.formReagendar = false;
    this.disciplinas = [];

    this.paises = [];
    this.ufs = [];
    this.municipios = [];

    this.legendas = [
      {
        label: 'Disponível',
        class: 'azul'
      },
      {
        label: 'Agendado',
        class: 'vermelho'
      },
      {
        label: 'Não disponível',
        class: 'cinza'
      }
    ];

    $templateCache.put('calendarEventsTemplate.html', templateEventsCalendar);
    calendarConfig.templates.calendarSlideBox = 'calendarEventsTemplate.html';

    // Cofigurações do calendário
    this.calendario = {
      calendarView: 'month',
      date: new Date(),
      isOpen: false,
      events: [],
      viewChangeClicked: (nextView) => {
        return nextView === 'year' || nextView === 'month';
      },
      timespanClicked: (date, cell) => {
        this.agendamento.id_avaliacaoaplicacao = null;

        if (cell.events.length) {
          const event = cell.events[0];
          const model = angular.copy(event.agendamento || this.ultimoAgendamento);

          if (!event.agendamento) {
            delete model.id_avaliacaoagendamento;
          }

          this.agendamento.id_avaliacaoagendamento = model.id_avaliacaoagendamento;
          this.agendamento.id_tipodeavaliacao = model.id_tipodeavaliacao;
          this.agendamento.id_avaliacao = model.id_avaliacao;
          this.agendamento.id_situacao = model.id_situacao;
          this.agendamento.nu_presenca = model.nu_presenca;
        }

        // Ao clicar em uma data que não está disponível para agendamento, apresentar a mensagem
        else {
          const msg = this.agendamento.id_municipio ? 'Data não disponível para agendamento.' : 'Selecione uma cidade.';
          this.NotifyService.notify('warning', '', msg);
        }
      }
    };

    this.alterarEtapa('endereco');
  }

  /**
   * @description Alternar entre as etapas do agendamento
   * @param etapa
   */
  alterarEtapa = (etapa) => {
    this.etapaAtual = this.etapas.indexOf(etapa);

    switch (etapa) {
    case 'endereco':
      if (!this.endereco.id_endereco) {
        this.loading = true;
        this.buscarEnderecoCorrespondencia().finally(() => {
          this.$q.all([
            this.buscarPaises(),
            this.buscarUfs(),
            this.buscarMunicipios(this.endereco.sg_uf)
          ]).finally(() => {
            this.loading = false;
          });
        });
      }
      break;
    case 'calendario':
      // Usar por padrão os locais do endereço de correspondência para pesquisar os polos de aplicação
      angular.extend(this.agendamento, {
        sg_uf: this.endereco.sg_uf,
        id_municipio: this.endereco.id_municipio
      });

      this.prepararCalendario();

      if (!this.disciplinas.length) {
        this.buscarDisciplinasAgendamento();
      }
      break;
    }
  }

  /**
   * @description Salvar endereço de correspondência
   * @param $event
   * @returns {boolean}
   */
  salvarEnderecoCorrespondencia = ($event) => {
    const form = $event.target;
    const is_valid = form.checkValidity();

    if (!is_valid) {
      return false;
    }

    this.loading = true;

    this.EnderecoService.getResource().update(this.endereco.id_endereco, this.endereco).$promise.then(
      (response) => {
        this.loading = false;

        // Mensagem de confirmação de Dados atualizados
        this.NotifyService.notify('success', '', 'Dados atualizados.');

        angular.extend(this.endereco, response);

        // Após a confirmação dos dados deve ocorrer o direcionamento automático para a tela de agendamento.
        this.alterarEtapa('calendario');
      },
      (error) => {
        this.loading = false;
        this.NotifyService.notify('error', '', error.data.message);
      }
    );
  }

  /**
   * @description Salvar agendamento
   * @returns {boolean}
   */
  salvarAgendamento = () => {
    if (!this.agendamento.id_avaliacaoaplicacao) {
      this.NotifyService.notify('warning', '', 'Por favor, selecione um local de prova.');
      return false;
    }

    this.formReagendar = false;
    this.loading = true;

    // Se for o primeiro agendamento do aluno, setar o valor padrão do parametro para PROVA FINAL (não recuperação)
    if (!this.agendamento.id_avaliacao) {
      this.agendamento.id_tipodeavaliacao = 0;
    }

    this.AgendamentoService.getResource().create(this.agendamento).$promise.then(
      (response) => {
        this.loading = false;
        angular.extend(this.agendamento, response);
        this.prepararCalendario();
        this.NotifyService.notify('success', '', 'Agendamento realizado.');
      },
      (error) => {
        this.loading = false;
        this.NotifyService.notify('error', '', error.data.message);
      }
    );
  }

  /**
   * @description Busca os agendamentos e as datas disponíveis e prepara o calendário
   */
  prepararCalendario = () => {
    this.calendario.events = [];
    this.ultimoAgendamento = {};
    this.buscarAgendamentos().finally(() => {
      this.eventos = this.calendario.events;
      if (this.agendamento.sg_uf && this.agendamento.id_municipio) {
        this.buscarDatasDisponiveis().finally(() => {
          this.eventos = this.calendario.events;
        });
      }
    });
  }

  /**
   * @description Mostra novamente as datas disponíveis para poder fazer o reagendamento
   */
  abrirFormReagendar = () => {
    this.formReagendar = this.agendamento.id_avaliacaoagendamento;
    this.prepararCalendario();
  }

  fecharFormReagendar = () => {
    this.formReagendar = false;
    this.prepararCalendario();
  }

  /**
   * @description Adiciona um evento ao calendário, verificando data e disponibilidade
   * @param item
   * @param config
   */
  adicionarEvento = (item, config = {status: 'disponivel'}) => {
    // Se for o mesmo agendamento que está sendo REAGENDADO, então deve ignorá-lo
    if (item.id_avaliacaoagendamento && Number(item.id_avaliacaoagendamento) === Number(this.formReagendar)) {
      return false;
    }

    // Verificar se já não possui algum AGENDAMENTO nesta data
    const exists = this.calendario.events.find((evento) => {
      return evento.data.id_avaliacaoagendamento && evento.data.dt_aplicacao === item.dt_aplicacao;
    });

    if (!exists) {
      // Se nao tiver data, nao adicionar
      if (!item.dt_aplicacao) {
        return false;
      }

      const date = new Date(Number(item.year), Number(item.month) - 1, Number(item.day), item.hr_inicio.substr(0, 2), item.hr_inicio.substr(3));

      // Se o status for "disponivel", o evento nao pode ser em dias passados
      // Porque não pode ser possível agendar uma avaliação em dias anteriores ao dia atual
      if (config.status === 'disponivel' && date < (new Date())) {
        return false;
      }

      // Se o ultimo agendamento for Agendado, nao pode mostrar nenhum dia disponivel ate que o aluno selecione Reagendar
      if (config.status === 'disponivel' && !this.formReagendar && Number(this.ultimoAgendamento.id_situacao) === this.SituacaoConstante.AGENDAMENTO_AGENDADO) {
        return false;
      }

      this.calendario.events.push(angular.extend({
        title: item.st_aplicadorprova,
        type: 'info',
        data: item,
        startsAt: date,
        endsAt: date,
        incrementsBadgeTotal: true,
        recursOn: 'year',
        cssClass: 'agendamento-status-' + config.status,
        allDay: false,
        color: { // can also be calendarConfig.colorTypes.warning for shortcuts to the deprecated event types
          primary: '#e3bc08', // the primary event color (should be darker than secondary)
          secondary: '#fdf1ba' // the secondary event color (should be lighter than primary)
        }
      }, config));
    }
  };

  /**
   * @description Buscar endereço de correspondência do aluno
   */
  buscarEnderecoCorrespondencia = () => {
    return this.PessoaService.getVwPessoaEndereco(this.$localStorage.user_id, this.TipoEnderecoConstante.CORRESPONDENCIA).then(
      (response) => {
        response.id_pais = response.id_pais || this.PaisConstante.BRASIL;
        angular.extend(this.endereco, response);
      }
    );
  }

  /**
   * @description Buscar a lista de disciplinas do agendamento disponível
   */
  buscarDisciplinasAgendamento = () => {
    this.loading = true;
    return this.AgendamentoService.getDisciplinasAgendamento(this.idMatricula).then(
      (response) => {
        this.loading = false;
        this.disciplinas = response;
      },
      () => {
        this.loading = false;
        this.disciplinas = [{st_tituloexibicao: 'Nenhuma disciplina encontrada.'}];
      }
    );
  }

  /**
   * @description Buscar as datas que possuem aplicações disponíveis
   */
  buscarDatasDisponiveis = () => {
    return this.AgendamentoService.getDatasDisponiveis(this.agendamento.sg_uf, this.agendamento.id_municipio).then(
      (response) => {
        response.forEach((item) => {
          this.adicionarEvento(item, {status: 'disponivel'});
        });
      }
    );
  }

  /**
   * @description Buscar o histórico de agendamento do aluno
   */
  buscarAgendamentos = () => {
    return this.AgendamentoService.getAll({
      id_matricula: this.idMatricula
    }).then(
      (response) => {
        this.ultimoAgendamento = response[response.length - 1];

        if (Number(this.ultimoAgendamento.id_situacao) === this.SituacaoConstante.AGENDAMENTO_AGENDADO) {

          this.agendamento.id_avaliacao = this.ultimoAgendamento.id_avaliacao;
          this.agendamento.id_avaliacaoagendamento = this.ultimoAgendamento.id_avaliacaoagendamento;
          this.agendamento.nu_presenca = this.ultimoAgendamento.nu_presenca;

          this.calendario.date = new Date(Number(this.ultimoAgendamento.year), Number(this.ultimoAgendamento.month) - 1, Number(this.ultimoAgendamento.day), this.ultimoAgendamento.hr_inicio.substr(0, 2), this.ultimoAgendamento.hr_inicio.substr(3));
          this.calendario.isOpen = true;
        }
        else if (this.agendamento.nu_presenca === 0 && Number(this.ultimoAgendamento.id_situacao) === this.SituacaoConstante.AGENDAMENTO_LIBERADO) {
          this.calendario.calendarView = 'year';
        }

        response.forEach((item) => {
          this.adicionarEvento(item, {
            status: 'agendado',
            agendamento: item
          });
        });
      }
    );
  }

  /**
   * @description Buscar a lista de países
   */
  buscarPaises = () => {
    return this.EnderecoService.getPaises().then(
      (response) => {
        this.paises = response;
      },
      (error) => {
        this.NotifyService.notify('error', '', error.data.message);
      }
    );
  }

  /**
   * @description Buscar a lista de ufs
   */
  buscarUfs = () => {
    return this.EnderecoService.getUfs().then(
      (response) => {
        this.ufs = response;
      },
      (error) => {
        this.NotifyService.notify('error', '', error.data.message);
      }
    );
  }

  /**
   * @description Buscar a lista de municípios baseado no uf
   * @param sgUf
   */
  buscarMunicipios = (sgUf) => {
    if (sgUf) {
      return this.EnderecoService.getMunicipios(sgUf).then(
        (response) => {
          this.municipios = response;
        },
        (error) => {
          this.NotifyService.notify('error', '', error.data.message);
        }
      );
    }
  }

  /**
   * @description Busca os dados de endereço baseado no CEP
   * @returns {boolean}
   */
  buscarEnderecoCep = () => {
    if (this.endereco.st_cep && this.endereco.st_cep.replace(/\D/g).length === 8) {
      this.EnderecoService.getByCep(this.endereco.st_cep).then(
        (response) => {
          response.id_pais = response.id_pais || this.PaisConstante.BRASIL;
          angular.extend(this.endereco, response);
          this.buscarMunicipios(this.endereco.sg_uf);
        },
        () => {
          this.NotifyService.notify('warning', '', 'CEP inválido, por favor tente novamente.');
        }
      );
    }

    return false;
  }

  /**
   * @description Abre em uma nova janela o comprovante de agendamento realizado
   * @param idAvaliacaoAgendamento
   */
  abrirComprovanteAgendamento = (idAvaliacaoAgendamento) => {
    this.$window.open(this.CONFIG.urlG2 + 'default/textos/comprovante-agendamento?id_usuario=' + this.$localStorage.user_id + '&id_avaliacaoagendamento=' + idAvaliacaoAgendamento, '_blank');
  }

  /**
   * @description Define se o botão "Confirmar" agendamento deve ser mostrado
   * @returns {boolean}
   */
  mostrarBotaoAgendamento = () => {
    if (!this.agendamento.id_avaliacaoaplicacao) {
      return false;
    }

    if (this.formReagendar || Number(this.agendamento.id_situacao) !== this.SituacaoConstante.AGENDAMENTO_AGENDADO) {
      return true;
    }

    return false;
  }

  /**
   * @description Define se o botão "Reagendar" deve ser mostrado
   * @returns {boolean}
   */
  mostrarBotaoReagendamento = () => {
    if (this.formReagendar) {
      return false;
    }

    if (Number(this.agendamento.id_avaliacao) !== this.ultimoAgendamento.id_avaliacao) {
      return false;
    }

    if (this.agendamento.id_avaliacaoagendamento === this.ultimoAgendamento.id_avaliacaoagendamento && this.agendamento.nu_presenca === null) {
      return true;
    }

    if (this.agendamento.nu_presenca === 0 && Number(this.ultimoAgendamento.id_situacao) === this.SituacaoConstante.AGENDAMENTO_LIBERADO) {
      return true;
    }

    return false;
  }

  alterarDataCalendario = (incMonth = 0, incYear = 0) => {
    const day = 1;
    const month = Number(this.calendario.date.getMonth()) + Number(incMonth);
    const year = Number(this.calendario.date.getFullYear()) + Number(incYear);

    this.calendario.date = new Date(year, month, day);

    // A lib do calendario coloca os eventos em TODOS os anos, então foi necessário filtrá-los aqui.
    this.calendario.events = this.eventos.filter((event) => {
      return Number(event.startsAt.getFullYear()) === year;
    });
  }

}

export default PwAgendamentoFormController;
