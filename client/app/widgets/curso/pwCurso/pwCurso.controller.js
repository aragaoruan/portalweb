class PwCursoController {
  constructor($state, CONFIG) {
    'ngInject';
    this.name = 'pwCurso';
    this.state = $state;
    this.config = CONFIG;

    // this.matriculaAluno = JSON.parse(localStorage.getItem('matricula.'+this.state.params.idMatriculaAtiva));
    this.matriculaAluno.st_imagemUrl = this.config.urlG2 + this.matriculaAluno.st_imagem;
  }
}

export default PwCursoController;
