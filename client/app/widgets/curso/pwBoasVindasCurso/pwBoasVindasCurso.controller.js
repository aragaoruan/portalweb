import loadingGif from "../../../../assets/img/loading.gif";

class PwBoasVindasCursoController {
  constructor(CONFIG, $state, CursoService, CursoAlunoService, $uibModal, $sce) {
    'ngInject';
    this.name = 'pwBoasVindasCurso';
    this.CursoAlunoService = CursoAlunoService;
    this.CursoService = CursoService;
    this.$state = $state;
    this.CONFIG = CONFIG;
    this.$sce = $sce;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.detalhesCoordenadorFetched = 0;
    this.boasVindasCursoFetched = 0;
    this.avatarExists = false;
    this.modalVideo = $uibModal;

    this.resumoBoasVindasObj = {
      idUsuarioCoordenador: '',
      textoBoasVindasCurso: '',
      nomeCoordenador: '',
      descricaoCoordenador: '',
      embedVideoApresentacao: '',
      urlAvatar: ''
    };
    this.getResumoBoasVindasCurso(this.matricula.id_curso);
  }

  checkIfAvatarExists = () => {
    const imgUrl = this.CONFIG.urlG2 + this.resumoBoasVindasObj.urlAvatar;
    const request = new XMLHttpRequest();
    request.open('GET', imgUrl, false);
    request.send();
    return request.status === 200;
  };

  openModalVideo = () => {
    this.modalVideo.open({
      template: '<div class="embed-responsive embed-responsive-16by9">' + this.resumoBoasVindasObj.embedVideoApresentacao + '</div>'
    });
  };

  /* Preenche o resumoBoasVindasObj com os dados
   relacionados à tela de boas vindas. */

  getResumoBoasVindasCurso = (idCursoAtivo) => {
    this.loading = true;
    this.CursoService.getCoordenadorCurso(idCursoAtivo)
      .then((success) => {
        const idCoordenadorCurso = success.id_usuario;
        const idEntidade = this.matricula.id_entidade;

        this.resumoBoasVindasObj.idUsuarioCoordenador = idCoordenadorCurso;

        //Busca e seta no objeto descrição e nome do coordenador.
        this.CursoService.getDetalhesCoordenador(idCoordenadorCurso, idEntidade)
          .then((success) => {
            this.resumoBoasVindasObj.descricaoCoordenador = success.st_descricaocoordenador;
            this.resumoBoasVindasObj.nomeCoordenador = success.st_nomeexibicao;

            //.substr(1) necessário para remover o primeiro '/' da string com a URL.
            if (success.st_urlavatar) {
              this.resumoBoasVindasObj.urlAvatar = success.st_urlavatar.substr(1);
            }

            this.detalhesCoordenadorFetched = true;
            this.avatarExists = this.checkIfAvatarExists();
          }, (() => {
          }));

      }, (() => {
      }));

    //Busca e seta no objeto o texto de boas vindas e o vídeo de apresentação.
    this.CursoService.getBoasVindasCurso(this.matricula.id_curso)
      .then((success) => {
        this.resumoBoasVindasObj.textoBoasVindasCurso = success.st_boasvindascurso;

        if (success.st_valorapresentacao && success.st_valorapresentacao !== "") {
          // Garantindo atributos de fullscreen no vídeo
          var myEl = angular.element(success.st_valorapresentacao);
          myEl.attr('allowfullscreen', "true");
          myEl.attr("webkitallowfullscreen", "true");
          myEl.attr("mozallowfullscreen", "true");
          this.resumoBoasVindasObj.embedVideoApresentacao = this.$sce.trustAsHtml(myEl[0].outerHTML);
        }

        this.boasVindasCursoFetched = true;

      }, () => {
      })
      .finally(() => {
        this.loading = false;
      });
  };

}


export default PwBoasVindasCursoController;
