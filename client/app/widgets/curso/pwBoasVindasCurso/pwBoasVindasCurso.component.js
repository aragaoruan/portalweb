import template from './pwBoasVindasCurso.html';
import controller from './pwBoasVindasCurso.controller';
import './pwBoasVindasCurso.scss';

const pwBoasVindasCursoComponent = {
  restrict: 'E',
  bindings: {
    matricula: '<'
  },
  template,
  controller,
  controllerAs: 'bvC'
};

export default pwBoasVindasCursoComponent;
