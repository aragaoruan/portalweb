import angular from 'angular';

// Padrão definido em http://confluence.unyleya.com.br/pages/viewpage.action?spaceKey=DEV&title=Desenvolvimento+Novo+Portal
import AbrirAtendimento from './atendimento/pwAbrirAtendimento/pwAbrirAtendimento';
import DescricaoCurso from './curso/pwCurso/pwCurso';
import Avisos from './avisos/pwAvisosList/pwAvisosList';
import Disciplina from './disciplina/pwDisciplinaList/pwDisciplinaList';
import pwMoodleCourseContents from './moodle/pwMoodleCourseContents/pwMoodleCourseContents';
import pwDisciplinaDescricao from './disciplina/pwDisciplinaDescricao/pwDisciplinaDescricao';
import DisciplinaAndamento from './disciplina/pwDisciplinaAndamento/pwDisciplinaAndamento';
import BoasVindasCurso from './curso/pwBoasVindasCurso/pwBoasVindasCurso';
import DisciplinaProfessor from './disciplina/pwDisciplinaProfessor/pwDisciplinaProfessor';
import MeusDocumentos from './documento/pwMeusDocumentos/pwMeusDocumentos';
import pwEnvioTccForm from './tcc/pwEnvioTccForm/pwEnvioTccForm';
import GradeNotas from './notas/pwGradeNotas/pwGradeNotas';
import GradeNotasDetalhamento from './notas/pwGradeNotasDetalhamento/pwGradeNotasDetalhamento';
import PrimeiroAcesso from './primeiro-acesso/pwPrimeiroAcesso/pwPrimeiroAcesso';
import pwAgendamentoForm from './agendamento/pwAgendamentoForm/pwAgendamentoForm';
import RecentActivity from './moodle/pwMoodleRecentActivity/pwMoodleRecentActivity';

export default angular.module('app.widgets', [
  pwMoodleCourseContents.name,
  AbrirAtendimento.name,
  DescricaoCurso.name,
  Avisos.name,
  Disciplina.name,
  pwDisciplinaDescricao.name,
  DisciplinaAndamento.name,
  BoasVindasCurso.name,
  DisciplinaProfessor.name,
  GradeNotas.name,
  MeusDocumentos.name,
  pwEnvioTccForm.name,
  PrimeiroAcesso.name,
  pwAgendamentoForm.name,
  RecentActivity.name,
  GradeNotasDetalhamento.name
]);
