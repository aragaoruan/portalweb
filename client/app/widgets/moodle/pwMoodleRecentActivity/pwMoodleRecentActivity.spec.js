import PwMoodleRecentActivityModule from './pwMoodleRecentActivity';
import PwMoodleRecentActivityController from './pwMoodleRecentActivity.controller';
import PwMoodleRecentActivityComponent from './pwMoodleRecentActivity.component';
import PwMoodleRecentActivityTemplate from './pwMoodleRecentActivity.html';

describe('PwMoodleRecentActivity', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwMoodleRecentActivityModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwMoodleRecentActivityController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwMoodleRecentActivityComponent;

    it('includes the intended template',() => {
        expect(component.template).to.equal(PwMoodleRecentActivityTemplate);
      });

    it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

    it('invokes the right controller', () => {
        expect(component.controller).to.equal(PwMoodleRecentActivityController);
      });
  });
});
