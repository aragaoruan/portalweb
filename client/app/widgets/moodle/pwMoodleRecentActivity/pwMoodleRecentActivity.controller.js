  class PwMoodleRecentActivityController {

    constructor(UnyleyaMoodleService,$state,NotifyService) {
      'ngInject';

      this.name = 'pwMoodleRecentActivity';
      this.UnyleyaMoodleService = UnyleyaMoodleService;
      this.$state = $state;
      this.toaster = NotifyService;

    /**
     * Os parâmetros idMatricula e idSaladeaula são passados via parâmetro para o widget, o idMatricula corresponde à Matrícula do Aluno no G2,
     * já o idSaladeaula corresponde ao course
     */

      if (this.idMatricula && this.idCurso && this.idSaladeaula) {
        this.ActivityContents(this.idMatricula, this.idCurso, this.idSaladeaula);
      }

      this.activities = [];
    }

    ActivityContents = (idMatricula, idCurso, idSaladeaula) => {
      this.UnyleyaMoodleService.listRecentActivity(idMatricula, idCurso, idSaladeaula).then((activities) => {
        this.activities = activities;
      }, (error) => {
        this.toaster.notify(error.data);
      });
    };

    /**
     * Retorna o icone para as atividade co moodle
     * @param  {string} modname Tipo de atividade do moodle
     * @return {string}         Class do icone
     */

    getIcon = (modname) => {
      switch (modname) {
      case 'forum':
        return 'icon-icon_forum';
      case 'page':
        return 'icon-icon_aula';
      case 'resource':
        return 'icon-icon_livro';
      case 'folder':
        return 'icon-icon_livro';
      case 'quiz':
        return 'icon-icon_disc';
      case 'assign':
        return 'icon-icon_disc';
      case 'label':
        return false;
      default:
        return 'icon-icon_aula';

      }
    };

}

  export default PwMoodleRecentActivityController;
