class PwMoodleCourseContentsController {

  constructor(MoodleService,$state,NotifyService,$q) {
    'ngInject';
    this.$q = $q;
    this.name = 'pwMoodleCourseContents';
    this.MoodleService = MoodleService;
    this.$state = $state;
    this.toaster = NotifyService;

    /**
    * Os parâmetros idMatricula e idCurso são passados via parâmetro para o widget, o idMatricula corresponde à Matrícula do Aluno no G2,
    * já o idCurso corresponde ao courseId do Moodle, pode ser pego na tb_saladeaulaintegracao campo st_codsistemacurso
    */
    if (this.idMatricula && this.idCurso && this.idSaladeaula) {

      /* Garante que a função que retorna a classe (colorida) e o texto do
      tooltip só será chamada quando as promises this.ActivitiesComplet() e
      this.CourseContents() já tenham retornado seus resultados.
      */

      const promises = [
        this.ActivitiesComplet(this.idMatricula, this.idCurso, this.idSaladeaula),
        this.CourseContents(this.idMatricula, this.idCurso, this.idSaladeaula)
      ];

      $q.all(promises).then(() => {
        this.getCompletionStatus();
      });

    }

    this.contents = [];
    this.statusActivities   = [];
    this.statusCourse   = [];
    this.errormessage = '';

  }

  /**
   * Retorna os status das atividades do aluno no moodle
   * @param idMatricula
   * @param idCurso
   * @param idSaladeaula
   * @constructor
   */
  ActivitiesComplet = (idMatricula, idCurso, idSaladeaula) => {
    return this.MoodleService.getActivitiesCompletionStatus(idMatricula, idCurso, idSaladeaula).then((status) => {
      this.statusActivities = status;
    });
  }

  /**
   * Retorna o conteúdo do curso do Moodle
   * @param idMatricula
   * @param idCurso
   * @param idSaladeaula
   * @constructor
   */
  CourseContents = (idMatricula, idCurso, idSaladeaula) => {
    return this.MoodleService.getCourseContents(idMatricula, idCurso, idSaladeaula).then((contents) => {

      // Define qual conteudo e módulo estão selecionados
      if (this.$state.params.idModulo) {
        contents.forEach((content) => {
          const moduloAtivo = content.modules.find((module) => {
            return Number(module.id) === Number(this.$state.params.idModulo);
          });

          if (moduloAtivo) {
            moduloAtivo.selected = true;
            content.selected = true;
          }
        });
      }

      this.contents = contents;
      this.errormessage = '';
    }, () => {
      this.errormessage = "A disciplina não está disponível. Favor entrar em contato através do Serviço de Atenção para maiores informações.";
    });
  }

  /**
   * Retorna o status do curso se no Moodle tiver sido definido algum critétio para isso
   * @param idMatricula
   * @param idCurso
   * @param idSaladeaula
   * @constructor
   */
  CourseCompletionStatus = (idMatricula, idCurso, idSaladeaula) => {
    this.MoodleService.getCourseCompletionStatus(idMatricula, idCurso, idSaladeaula).then((contents) => {
      this.contentsStatus = contents;
    }, () => {

      // falta definir o que mostrar ao ter alguma mensagem de erro
    });
  }

  /**
  * Retorna o icone para as atividade co moodle
  * @param  {string} modname Tipo de atividade do moodle
  * @return {string}         Class do icone
  */
  getIcon = (modname) => {
    switch (modname) {
    case 'forum':
      return 'icon-icon_forum';
    case 'page':
      return 'icon-icon_aula';
    case 'resource':
      return 'icon-icon_livro';
    case 'folder':
      return 'icon-icon_livro';
    case 'quiz':
      return 'icon-icon_disc';
    case 'assign':
      return 'icon-icon_disc';
    case 'label':
      return false;
    default:
      return 'icon-icon_aula';

    }
  };

  /**
  * Redireciona a disciplina para sua tela main
  * @param objDisciplina
  */
  redirecionaDisciplina = () => {
    try {
      this.$state.go("app.disciplina", {
        idMatriculaAtiva: this.idMatricula,
        idSalaDeAula: this.idSaladeaula
      });
    }
    catch (e) {
      this.toaster.notify('error', 'Ops', 'Erro ao tentar redirecionar para tela de disciplina.');
    }
  }

  /* Adiciona no objeto content as propriedades
  moduleStatusCompletionClass (módulo), activityStatusCompletionClass e
  activityStatusCompletionText (ativiades). Utilizado para definir a classe
  com cor indicando o progresso e o texto exibido no tooltip.
  */
  getCompletionStatus = () => {

    //Módulo
    {
      angular.forEach(this.contents, (contentValue) => {
        if (contentValue.modules.length > 0) {
          contentValue.moduleStatusCompletionClass = 'curso-completo';
          angular.forEach(contentValue.modules, (modulo) => {
            angular.forEach(this.statusActivities.statuses, (status) => {
              if ((status.cmid === modulo.id) && status.timecompleted === 0) {
                contentValue.moduleStatusCompletionClass = 'curso-incompleto';
              }
            });
          });
        }
        else {
          contentValue.moduleStatusCompletionClass = 'curso-indisponivel';
        }
      });
    }

    //Atividades
    {
      angular.forEach(this.contents, (modulo) => {
        angular.forEach(modulo.modules, (atividade) => {
          let temAcompanhamento = false;
          angular.forEach(this.statusActivities.statuses, (status) => {
            if (status.cmid === atividade.id) {
              temAcompanhamento = true;
              if (status.timecompleted) {
                atividade.activityStatusCompletionClass = 'atividade-completa';
                atividade.activityStatusCompletionText = 'Atividade Concluída';
              }
              else {
                atividade.activityStatusCompletionClass = 'atividade-incompleta';
                atividade.activityStatusCompletionText = 'Atividade Pendente';
              }
            }
          });
          if (!temAcompanhamento) {
            atividade.activityStatusCompletionClass = 'atividade-sem-acompanhamento';
            atividade.activityStatusCompletionText = 'Atividade não acompanhada';
          }
        });
      });
    }
  }

}

export default PwMoodleCourseContentsController;
