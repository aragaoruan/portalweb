import template from './pwGradeNotasDetalhamento.html';
import controller from './pwGradeNotasDetalhamento.controller';
import './pwGradeNotasDetalhamento.scss';

const pwGradeNotasDetalhamentoComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pwGradeNotasDetalhamentoComponent;
