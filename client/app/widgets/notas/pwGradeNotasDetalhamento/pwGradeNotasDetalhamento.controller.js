import loadingGif from "../../../../assets/img/loading.gif";

const _ = require('lodash');

class PwGradeNotasDetalhamentoController {
  constructor(CONFIG, NotasService, $state) {
    "ngInject";

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.name = 'pcGradeNotasForm';
    this.NotasService = NotasService;
    this.$state = $state;
    this.notas = [];
    this.erro = '';
    this.alerta = '';
    this.exibirGrade = {};
    this.stringsLegenda = null;
    this.getGradeNotasDetalhada();
  }

  /** Busca todas as notas da matrícula e formata os dados para exibição
   * correta na grade de notas.
   * @function getGradeNotas
   */

  getGradeNotasDetalhada = () => {

    this.loading = true;
    this.NotasService.getGradeNotasDetalhada(this.$state.params.idMatriculaAtiva, this.$state.params.idDisciplina)
      .then((success) => {
        this.notas = success;
        this.exibirGrade.graduacao = 0;

        /* Itera sobre as notas de cada módulo, verificando a existência
         de notas de recuperação / atividade. Valores salvos nas variáveis
         são utilizados para manipular a exibição das respectivas colunas
         no template. */

        _.forEach(this.notas, (value) => {
          _.forEach(value.disciplinas, (disciplina) => {

            this.alerta = null;

            if (disciplina.ar_notasmoodle === null) {
              this.alerta = 'Houve umproblema ao recuperar as notas, por favor recarregue a página apertando F5';
            }

            if (disciplina.st_avaliacaorecuperacao !== null) {
              value.temNotaRecuperacao = 1;
            }
            if (disciplina.st_avaliacaoatividade !== null) {
              value.temNotaAtividade = 1;
            }
            if (disciplina.st_notaead_prr) {
              value.temNotaPrrEad = 1;
            }
            if (disciplina.st_notatcc) {
              value.temNotaTcc = 1;
            }
            if (disciplina.st_notatcc_prr) {
              value.temNotaPrrTcc = 1;
            }
          });
        });

        /* Checa se existe mais de um módulo.
         Caso possua múltiplos módulos, o curso é de graduação.
         Caso negativo, o curso é de pós graduação. */

        /* O array resource retorna inicialmente duas chaves extras,
         promise e proto. Caso haja apenas um módulo no retorno, o array
         tem tamanho 3. */

        if (_.size(this.notas) > 3) {
          this.exibirGrade.graduacao = 0;
        }
        else {
          this.exibirGrade.graduacao = 0;
        }

        this.getStringsLegenda(this.exibirGrade.graduacao);

      }, (error) => {
        this.erro = error.data.message;
      }).finally(() => {
        this.loading = false;
      });
  };

  /** Retorna strings de legenda das cores (utilizadas na tabela de legendas)
   * com base no tipo de curso.
   * @function getStringsLegenda
   * @param {boolean} tipoCurso - Tipo do curso. 1 - Graduação, 0 - Pós graduação.
   */

  getStringsLegenda = (tipoCurso) => {
    if (tipoCurso === 1) {
      this.stringsLegenda = {
        Aprovado: {
          classeCor: 'verde',
          classeLegenda: 'verde-legenda'
        },
        Reprovado: {
          classeCor: 'vermelho',
          classeLegenda: 'vermelho-legenda'
        }
      };
    }

    //Caso pós graduação.
    else {
      this.stringsLegenda = {
        Aprovado: {
          classeCor: 'verde',
          classeLegenda: 'verde-legenda'
        },
        Satisfatório: {
          classeCor: 'azul',
          classeLegenda: 'azul-legenda'
        },
        Insatisfatório: {
          classeCor: 'vermelho',
          classeLegenda: 'vermelho-legenda'
        }
      };
    }
  };

  /**
   * Retorna um hífen (-) caso o valor da nota seja null.
   * @param {string} nota - Nota a ser formatada.
   * @return {string}
   */
  formataNota = (nota) => {
    return nota !== null ? nota : nota = '-';
  }

  /** Retorna uma variável boolean informando se há ou não nota final
   * PRR. Serve como condição para exibir corretamente as cores do status
   * e a nota final PRR quando existir.
   * @function temNotaFinalPRR
   * @param {object} disciplina - Objeto com os dados da disciplina.
   * @returns boolean
   * */

  temNotaFinalPRR = (disciplina) => {
    return (disciplina.id_disciplina_prr !== '' && disciplina.st_notaead_prr !== null);
  };

  /***
   * Exibe a nota final adequada, seja ela a nota final ou
   * a nota final PRR.
   * @param {object} disciplina - Objeto com os dados da disciplina.
   * @return {string}
   */

  exibeNotaFinal = (disciplina) => {

    const temNotaFinalPRR = this.temNotaFinalPRR(disciplina);

    if (temNotaFinalPRR) {
      if (disciplina.nu_notafinal_prr === 0 || disciplina.nu_notafinal_prr === null) {
        return '-';
      }
      if (disciplina.nu_notafinal_prr !== 0 && disciplina.nu_notafinal_prr !== null) {
        return disciplina.nu_notafinal_prr;
      }
    }

    else {
      if (disciplina.nu_notafinal === 0 || disciplina.nu_notafinal === null) {
        return '-';
      }
      if (disciplina.nu_notafinal !== 0 && disciplina.nu_notafinal !== null) {
        return disciplina.nu_notafinal;
      }
    }
  };

}

export default PwGradeNotasDetalhamentoController;
