import PwGradeNotasModule from './pwGradeNotas';
import PwGradeNotasController from './pwGradeNotas.controller';
import PwGradeNotasComponent from './pwGradeNotas.component';
import PwGradeNotasTemplate from './pwGradeNotas.html';

describe('PwGradeNotas', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwGradeNotasModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwGradeNotasController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwGradeNotasComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwGradeNotasTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwGradeNotasController);
    });
  });
});
