import loadingGif from "../../../../assets/img/loading.gif";

// import popoverOptions from popoverOptions;

class PwAbrirAtendimentoController {
  constructor(CONFIG, OcorrenciaService, DisciplinaAlunoService, $state, NotifyService, $scope, $document, $localStorage, $rootScope, AuthService, Upload, MensagemPadrao) {
    'ngInject';

    this.name = 'pwAbrirAtendimento';
    this.state = $state;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.document = $document;
    this.storage = $localStorage;
    this.AuthService = AuthService;
    this.Upload = Upload;
    this.MensagemPadrao = MensagemPadrao;

    this.OcorrenciaService = OcorrenciaService;
    this.DisciplinaAlunoService = DisciplinaAlunoService;

    this.toaster = NotifyService;
    this.loadingGif = loadingGif;
    this.loading = false;

    this.ocorrencia = {};
    this.assuntosOcorrencia = [];
    this.subAssuntosOcorrencia = [];
    this.matriculaAluno = [];
    this.salasAluno = [];

    //carrega o inicio do formulario
    this.loadDataForm();

    // this.popoverOptions = popoverOptions;

    this.arquivo_ocorrencia = null;
    this.enviandoArquivo = false;

    //Ao fechar registra o log da funcionalidade
    this.AuthService.gravaLogAcesso('app.novo-atendimento', this.state.params);
  }

//NO portal WEB só é possivel ter uma matriculaAluno
//Carrega as salas e os assuntos de acordo com o id_matricula e id_entidade, respectivamente
  loadDataForm = () => {
    this.loading = true;
    this.ocorrencia = {};
    this.assuntosOcorrencia = [];
    this.subAssuntosOcorrencia = [];
    this.matriculaAluno = [];
    this.matriculaAluno = angular.fromJson(this.storage['matricula.' + this.state.params.idMatriculaAtiva]);
    this.ocorrencia.id_matricula = this.state.params.idMatriculaAtiva;
    this.ocorrencia.id_entidade = this.matriculaAluno.id_entidade;

    this.getSalaAulaAlunoByMatricula(this.matriculaAluno);
    this.getAssuntosOcorrencia();
  };

  /**
   ** Retorna salas de aula por matricula do aluno
   **/
  getSalaAulaAlunoByMatricula = (matricula) => {
    this.loading = true;

    this.DisciplinaAlunoService.getResource().getAll({
      id_entidade: matricula.id_entidade,
      id_matricula: matricula.id_matricula,
      id_projeto: matricula.id_curso
    }, (disciplinas) => {
      this.salasAluno = disciplinas;
      this.loading = false;
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });
  };

  /**
   ** Retorna assuntos da entidade
   **/
  getAssuntosOcorrencia = () => {
    this.loading = true;

    this.OcorrenciaService.getResource().assuntos({
      id_matricula: this.matriculaAluno,
      bl_interno: 0
    }, (assuntos) => {
      this.assuntosOcorrencia = assuntos;
      this.loading = false;
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });
  };

  /**
   ** Busca subassuntos depois do change do combo de assuntos
   **/
  getSubassunto = () => {
    const id_assuntocopai = this.ocorrencia.id_assuntopai;

    if (!id_assuntocopai) {
      this.subAssuntosOcorrencia = [];
      return;
    }

    this.loading = true;

    this.OcorrenciaService.getResource().assuntos({
      id_assuntocopai: id_assuntocopai,
      bl_interno: 0
    }, (subassuntos) => {
      this.subAssuntosOcorrencia = subassuntos;
      this.loading = false;
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });
  };

  /**
   ** Fecha o popover-atendimento
   **/
  closePopover = () => {
    this.$rootScope.$emit('closePopover');
  };

  /**
   * Salva ocorrencia, parâmetros e arquivo anexado
   */
  save = () => {

    this.loading = true;

    this.OcorrenciaService.create(this.ocorrencia).success(() => {

      this.loading = false;
      this.toaster.notify('success', 'Sucesso', 'Ocorrência salva com sucesso!');
      this.loadDataForm();
      this.closePopover();

    }).error((error) => {
      this.loading = false;
      this.toaster.notify(error);
    });
  };

  /**
   * Recebe um arquivo e faz as verificaçoes necessarias para permitir o upload
   * caso fique fora do permitido limpa o campo para nao enviar o arquivo
   */
  verificacaoArquivo = () => {

    if (this.ocorrencia.files) {
      var formatosPermitidos = ["application/msword", "application/pdf", "image/png", "image/jpg", "image/jpeg", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"];

      if (formatosPermitidos.indexOf(this.ocorrencia.files.type) === -1) {
        this.toaster.notify('error', this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_FORA_DO_FORMATO_PERMITIDO);
        this.ocorrencia.files = {};
      }

      if (this.ocorrencia.files.size > 10000000) {
        this.toaster.notify('error', this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_TAMANHO);
        this.ocorrencia.files = {};
      }

    }
  }
}

export default PwAbrirAtendimentoController;
