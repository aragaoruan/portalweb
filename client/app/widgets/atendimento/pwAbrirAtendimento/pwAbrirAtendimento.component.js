import template from './pwAbrirAtendimento.html';
import controller from './pwAbrirAtendimento.controller';
import './pwAbrirAtendimento.scss';

const pwAbrirAtendimentoComponent = {
  restrict: 'E',
  bindings: {
    popoverOptions : '='
  },
  template,
  controller,
  controllerAs: 'pwoc'
};

export default pwAbrirAtendimentoComponent;
