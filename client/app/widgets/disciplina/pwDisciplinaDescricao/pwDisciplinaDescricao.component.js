import template from './pwDisciplinaDescricao.html';
import controller from './pwDisciplinaDescricao.controller.js';
import './pwDisciplinaDescricao.scss';

const pwDisciplinaDescricaoComponent = {
  restrict: 'E',
  bindings: {
    disciplina: '='
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pwDisciplinaDescricaoComponent;
