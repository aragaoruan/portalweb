import PwDisciplinaDescricaoModule from './pwDisciplinaDescricao';
import PwDisciplinaDescricaoController from './pwDisciplinaDescricao.controller.js';
import PwDisciplinaDescricaoComponent from './pwDisciplinaDescricao.component.js';
import PwDisciplinaDescricaoTemplate from './pwDisciplinaDescricao.html';

describe('PwDisciplinaDescricao', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwDisciplinaDescricaoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwDisciplinaDescricaoController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwDisciplinaDescricaoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwDisciplinaDescricaoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwDisciplinaDescricaoController);
    });
  });
});
