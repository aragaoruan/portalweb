import PwDisciplinaAndamentoModule from "./pwDisciplinaAndamento";
import PwDisciplinaAndamentoController from "./pwDisciplinaAndamento.controller";
import PwDisciplinaAndamentoComponent from "./pwDisciplinaAndamento.component";
import PwDisciplinaAndamentoTemplate from "./pwDisciplinaAndamento.html";

describe('PwDisciplinaAndamento', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwDisciplinaAndamentoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwDisciplinaAndamentoController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PwDisciplinaAndamentoComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PwDisciplinaAndamentoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwDisciplinaAndamentoController);
    });
  });
});
