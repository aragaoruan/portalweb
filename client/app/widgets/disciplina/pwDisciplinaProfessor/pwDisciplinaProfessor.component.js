import template from "./pwDisciplinaProfessor.html";
import controller from "./pwDisciplinaProfessor.controller";
import "./pwDisciplinaProfessor.scss";

const pwDisciplinaProfessorComponent = {
  restrict: 'E',
  bindings: {
    idMatricula: '<',
    idSaladeaula: '<',
    idEntidade: '<',
    hasProfessor: '=?'
  },
  template,
  controller,
  controllerAs: 'pwCtrl'
};

export default pwDisciplinaProfessorComponent;
