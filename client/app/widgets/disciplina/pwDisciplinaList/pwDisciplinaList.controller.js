import loadingGif from "../../../../assets/img/loading.gif";
import templateModal from "../../../widgets/primeiro-acesso/pwPrimeiroAcesso/pwPrimeiroAcesso.html";
import modalPrimeiroAcessoController from "../../../widgets/primeiro-acesso/pwPrimeiroAcesso/pwPrimeiroAcesso.controller";

class pwDisciplinaListController {
  constructor(DisciplinaAlunoService, $state, NotifyService, $q, $scope, $uibModal, MensagemPadrao, $localStorage, CursoAlunoService, EvolucaoMatriculaConstante, MensagensInfoEvolucaoConstante) {
    'ngInject';
    this.name = 'pwDisciplinaList';
    this.service = DisciplinaAlunoService;
    this.cursoAlunoService = CursoAlunoService;
    this.$state = $state;
    this.$scope = $scope;
    this.modal = $uibModal;
    this.MensagemPadrao = MensagemPadrao;

    this.toaster = NotifyService;
    this.$q = $q;

    this.matriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);
    this.MensagensInfoEvolucaoConstante = MensagensInfoEvolucaoConstante;
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;
    this.disciplinas = [];

    this.dadosUsuario = {
      stNomeCurto: $localStorage.user_name.substr(0, $localStorage.user_name.indexOf(" "))
    };

    this.retornarDisciplinas();
  }

  //abre a modal de primeiro acesso
  abrirPrimeiroAcesso (){
    this.modal.open({
      template: templateModal,
      controller: modalPrimeiroAcessoController,
      controllerAs: 'pwpa',
      size: 'lg',
      scope: this.$scope,
      resolve: {
        primeiroacesso: () => {
          return this.matriculaAtiva;
        }
      }
    });
  }

  /**
   * Retorna as disciplinas com seus dados do moodle
   */
  retornarDisciplinas = () => {
    //inicia a variavel do loading para mostrar o gif
    this.loading = true;
    this.service.getAll({
      id_entidade: this.matriculaAtiva.id_entidade,
      id_matricula: this.matriculaAtiva.id_matricula,
      id_projetopedagogico: this.matriculaAtiva.id_curso
    }, true).then((disciplinasResp) => {
      this.disciplinas = disciplinasResp;

      //percorre os dados retornados
      angular.forEach(disciplinasResp, (disciplina) => {
        this.andamentoDisciplina(disciplina);
      });
    }).finally(() => {

      /* Matriculas com status trancado só devem exibir disciplinas cursadas.
      Casting necessário, já que o ESLinter obriga
      a utilização de operador de equalidade type-safe.*/

      if (Number(this.matriculaAtiva.id_evolucao) === this.EvolucaoMatriculaConstante.TRANCADO) {
        this.removeDisciplinasIncompletas();
      }
      this.loading = false;
    });
  }

  andamentoDisciplina = (disciplina) => {
    // Seta variavel boolean no objeto, indicando se a sala está ou não completa.
    disciplina.salaCompleta = disciplina.st_statuscomplete === 'Completo';

    //busca os andamentos da disciplina
    this.service.getAll({
      id_entidade: this.matriculaAtiva.id_entidade,
      id_matricula: this.matriculaAtiva.id_matricula,
      id_projetopedagogico: this.matriculaAtiva.id_curso,
      id_saladeaula: disciplina.id_saladeaula,
      bl_moodle: 1
    }, true).then((response) => {
      //se tiver retornado relevantes do moodle, a gente adiciona no scope
      if (response[0].ar_moodle && response[0].ar_moodle.ar_atividades) {
        disciplina.ar_moodle = response[0].ar_moodle;
        disciplina.st_statuscomplete = response[0].st_statuscomplete;
      }
      disciplina.showChart = true;
    });

  }

  /**
   * Redireciona a disciplina para sua tela main
   * @param objDisciplina
   */
  redirecionaDisciplina = (objDisciplina) => {
    try {
      console.log(objDisciplina);
      if (objDisciplina.id_status === 3){
        this.toaster.notify('warning', '', 'Disciplina ainda não iniciada');
      }
      else {

        this.$state.go("app.disciplina", {
          idMatriculaAtiva: this.matriculaAtiva.id_matricula,
          idSalaDeAula: objDisciplina.id_saladeaula
        });
      }
    }
    catch (e) {
      this.toaster.notify('error', 'Ops', 'Erro ao tentar redirecionar para tela de disciplina.');
    }
  }

  /**
   * Remove disciplinas incompletas da lista.
   */
  removeDisciplinasIncompletas = () => {
    angular.forEach(this.disciplinas, (value, key) => {
      if (!value.salaCompleta){
        this.disciplinas.splice(key, 1);
      }
    });
  }

  /* Funções utilizadas para manipulação do template.
  Evita inconsistências com ng-if. */

  evolucaoEhBloqueada = () => {
    return Number(this.matriculaAtiva.id_evolucao) === Number(this.EvolucaoMatriculaConstante.BLOQUEADO);
  }

  evolucaoEhTrancada = () => {
    return Number(this.matriculaAtiva.id_evolucao) === Number(this.EvolucaoMatriculaConstante.TRANCADO);
  }


}

export default pwDisciplinaListController;
