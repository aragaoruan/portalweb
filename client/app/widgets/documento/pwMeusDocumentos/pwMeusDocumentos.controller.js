import loadingGif from '../../../../assets/img/loading.gif';

class pwMeusDocumentosController {
  constructor($state, DownloadService, $localStorage, $window, $q, $scope, $sce, CONFIG, CursoAlunoService, FinanceiroService, VistaDeProvaService, NotifyService, TabelaPrecoService, AgendamentoService) {
    'ngInject';

    this.FinanceiroService = FinanceiroService;
    this.TabelaPrecoService = TabelaPrecoService;
    this.AgendamentoService = AgendamentoService;
    this.VistaDeProvaService = VistaDeProvaService;

    this.name = 'pwMeusDocumentos';
    this.$state = $state;
    this.DownloadService = DownloadService;
    this.storage = $localStorage;
    this.$window = $window;
    this.$scope = $scope;
    this.$q = $q;
    this.$sce = $sce;
    this.CONFIG = CONFIG;
    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);
    this.loadingGif = loadingGif;
    this.loading = true;
    this.resultTabelaPreco = false;
    this.toaster = NotifyService;
    this.manualDoAluno = null;
    this.agendamentoComprovante = null;
    this.bl_minhasprovas = null;

    this.retornaContratos();
    this.getTabelaPreco(this.MatriculaAtiva.id_entidade);
    this.getManualDoAluno(this.MatriculaAtiva.id_entidade);
    this.getAgendamentoComprovante(this.MatriculaAtiva.id_matricula);
    this.verificaPermissaoVistaDeProva(this.MatriculaAtiva.id_entidade);

    //armazena todos os contratos encontrados
    this.contratos = [];
  }

  /**
   * Gera a Url e injeta no iframe para efetuar o download do imposto de renda.
   */
  impostoDeRenda = () => {
    const idMatricula = this.$state.params.idMatriculaAtiva;
    const idEntidade = this.MatriculaAtiva.id_entidade;
    const downloadAction = 'imposto-renda';

    this.DownloadService.getAuth(downloadAction).then((auth) => {
      const urlDownload = this.DownloadService.downloadImpostoRenda(auth, downloadAction, idMatricula, idEntidade);
      this.$scope.detailFrame = this.$sce.trustAsResourceUrl(urlDownload);
    }, () => {

      // Error
    });
  }

  /**
   * Gera a Url e injeta no iframe para efetuar o download da tabela de preço da entidade
   */
  tabelaPreco = () => {
    const idEntidade = this.MatriculaAtiva.id_entidade;
    const downloadAction = 'tabela-preco';

    this.DownloadService.getAuth(downloadAction).then((auth) => {
      const urlDownload = this.DownloadService.geraUrlDownloadTabelaPreco(auth, downloadAction, idEntidade);
      this.$scope.detailFrame = this.$sce.trustAsResourceUrl(urlDownload);
    }, () => {
      this.toaster.notify('error', 'Erro', 'Não foi possível localizar configurações para tabela de preço.');
    });
  }

  /**
   * Verifica se há plano de pagamento disponível para a matrícula. Se houver,
   * gera e abre a URL para download do arquivo em PDF.
   * Caso contrário, notifica o usuário.
   */
  geraPdfPlanoPagamento = () => {
    const idMatricula = this.$state.params.idMatriculaAtiva;
    const idEntidade = this.MatriculaAtiva.id_entidade;
    const downloadAction = 'plano-pagamento';

    this.FinanceiroService.getStatusPlanoPagamento(idMatricula, idEntidade).then((response) => {
      //Se existir plano de pagamento, gerar url e baixar em PDF.
      if (response.existePlanoPagamento) {
        this.DownloadService.getAuth(downloadAction).then((auth) => {
          const urlDownload = this.DownloadService.geraUrlDownloadPlanoPagamento(auth, downloadAction, idMatricula, idEntidade);
          this.$scope.detailFrame = this.$sce.trustAsResourceUrl(urlDownload);
        }, () => {

          // Error
        });
      }
      else {
        //Se não existir plano de pagamento, exibe erro com detalhes do status.
        this.toaster.notify('error', 'Erro', response.status);
      }
    }, () => {

      //Error
    });
  }

  /**
   * Gera a URL com o token, apos faz a requisiçao solicitando os contratos
   * caso exista retonar um json com os documentos e o caminho, depois e feito o each na aplicaçao
   * imprimindo os contratos na tela
   */
  retornaContratos = () => {
    const idEntidade = this.MatriculaAtiva.id_entidade;
    const idUsuario = this.storage.user_id;
    const idMatricula = this.$state.params.idMatriculaAtiva;
    const downloadAction = 'contrato';

    this.DownloadService.getAuth(downloadAction).then((auth) => {
      const urlContrato = this.DownloadService.geraUrlDownloadContrato(auth, downloadAction, idEntidade, idUsuario, idMatricula);
      this.DownloadService.pesquisaContrato(urlContrato).then((response) => {
        this.contratos = response.data;
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    });
  }

  /**
   * Pesquisa se existe tabela de preço
   * @param id_entidade
   */
  getTabelaPreco = (id_entidade) => {
    this.TabelaPrecoService.getTabelaPreco(id_entidade).then((response) => {
      //verifica se o meio de pagamento é boleto.
      if (response[0].type) {
        this.resultTabelaPreco = true;
      }
    }, (error) => {
      this.toaster.notify(error.data);
    });
  }

  /** Verifica se há manual do aluno para a entidade. Caso positivo,
   * retorna id_upload e st_upload do manual do aluno.
   * @param {string} id_entidade - ID da entidade.
   * */

  getManualDoAluno(id_entidade) {
    this.FinanceiroService.getManualDoAluno(id_entidade).then((success) => {
      this.manualDoAluno = success;
    });
  }

  /** Monta a URL de download do manual do aluno, com base no st_upload.
   * @deprecated
   */
  downloadManualDoAluno() {
    const urlDownload = this.CONFIG.urlG2 + 'upload/manual/' + this.manualDoAluno.st_upload;
    this.$scope.detailFrame = this.$sce.trustAsResourceUrl(urlDownload);
  }

  /**
   * @description Buscar o ultimo agendamento ativo, para mostrar o comprovante
   * @param idMatricula
   */
  getAgendamentoComprovante(idMatricula) {
    this.AgendamentoService.getAgendamentoComprovante(idMatricula).then((response) => {
      this.agendamentoComprovante = response;
    });
  }

  /**
   * @description Abrir em uma nova janela o comprovante de agendamento realizado
   */
  abrirComprovanteAgendamento = () => {
    this.$window.open(this.CONFIG.urlG2 + 'default/textos/comprovante-agendamento?id_usuario=' + this.storage.user_id + '&id_avaliacaoagendamento=' + this.agendamentoComprovante.id_avaliacaoagendamento, '_blank');
  };

  /**
   * Função responsável por verificar se a entidade do aluno tem permissão para exibir
   * Ou não o botão minhas provas
   * POR-395
   */
  verificaPermissaoVistaDeProva = (idEntidade) => {
    this.VistaDeProvaService.getVerificarPermissao(idEntidade).then((response) => {
      if (response.bl_minhasprovas) {
        this.bl_minhasprovas = true;
      }
    });
  };

  /**
   * Função que vai consultar API, para verificar se o aluno possui alguma prova para revisão.
   * Se possuir, será redirecionado para o sistema da Fábrica de Prova.
   * Se não possuir, receberá um alerta informando.
   */
  vistaDeProva = () => {
    this.VistaDeProvaService.getVistaDeProva(this.storage.user_id).then((response) => {
      if (!response.message) {
        this.toaster.notify('warning', 'Por favor, verique se os pop-ups estão liberados');
        this.$window.open(response.url);
      }
      else {
        this.toaster.notify(response.type, response.message);
      }

    }, (error) => {
      this.toaster.notify(error.type, error.message);
    });
  };

}

export default pwMeusDocumentosController;
