import template from './pwMeusDocumentos.html';
import controller from './pwMeusDocumentos.controller';
import './pwMeusDocumentos.scss';

const pwMeusDocumentosComponent = {
  restrict: 'E',
  bindings: {
    exibirTitulo: '<'
  },
  template,
  controller,
  controllerAs: 'pwmd'
};

export default pwMeusDocumentosComponent;
