import PwAvisosListModule from './pwAvisosList';
import PwAvisosListController from './pwAvisosList.controller';
import PwAvisosListComponent from './pwAvisosList.component';
import PwAvisosListTemplate from './pwAvisosList.html';

describe('PwAvisosList', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwAvisosListModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwAvisosListController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwAvisosListComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwAvisosListTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwAvisosListController);
    });
  });
});
