import loadingGif from "../../../../assets/img/loading.gif";

class PwAvisosListController {
  constructor(AvisosService, $state, $compile, $scope) {
    'ngInject';
    this.name = 'pwAvisosList';

    this.compile = $compile;

    this.scope = $scope;

    //inicia a varaivel vazia dos avisos
    this.mensagens = [];

    //cria a variavel para a service
    this.AvisosService = AvisosService;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    //pega o state
    this.state = $state;

    //cria uma variavel no escopo para a matricula ativa
    this.idMatriculaAtiva = $state.params.idMatriculaAtiva;

    this.carregaMensagens();
  }

  /**
   * Carrega todos os avisos
   */
  carregaMensagens = () => {

    //inicia a variavel do loading para mostrar o gif
    this.loading = true;

    //pega os avisos
    this.AvisosService.getAll({id_matricula: this.idMatriculaAtiva}).then((response) => {

      //percorre os avisos setando os respectivos icones e outros
      angular.forEach(response, (aviso) => {
        aviso.icon = this.AvisosService.getIconAviso(aviso.type);
        this.mensagens.push(aviso);
      });

    }).finally(() => {
      this.loading = false;
    });
  }

  /**
   * Define os parametros e rota para redirecionar o aviso
   * @param aviso
   */
  redirecionarAviso = (aviso, index) => {
    var functionAviso = this.AvisosService.getFunctionAviso(aviso, this.idMatriculaAtiva, this);
    if (aviso.type === 'envioTcc') {
      functionAviso().result.then((result) => {

        if (result.type === "success") {
          this.mensagens.splice(index, 1);
        }

      });
    }
    else {
      functionAviso();
    }

  }

  /**
   * Funcao para alterar a varaivel que muda o state
   */
  changeState = () => {
    this.statusState = !this.statusState;
  }
}

export default PwAvisosListController;
