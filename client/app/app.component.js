import template from './app.html';
import 'normalize.css';
import './scss/common.scss';

const appComponent = {
  template,
  restrict: 'E'
};

export default appComponent;
