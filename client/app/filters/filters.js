import angular from 'angular';


const filtersModule = angular.module('app.filters', [])

  .filter('capitalize', () => {
    return function(input) {

      // Precisa mesmo do parâmetro all ?

      const alwaysToLower = ["em", "de", "e", "é", "a", "o", "da", "do", "dos", "das", "na", "com", "por", "para"]; // adicionar aqui todas as palavras que normalmente não precisam de alterações
      const words = input.split(' ');
      const capitalized = words.map((word) => {
        if (alwaysToLower.indexOf(word.toLowerCase().trim()) > -1) {
          return word.toLowerCase();
        }
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
      });
      return capitalized.join(' ');
    };
  })
  .filter('removeHtmlTags', () => {
    return function(text) {
      let html = String(text).replace(/<[^>]+>/gm, '');
      html = String(html).replace("<style>", "");
      html = String(html).replace("&nbsp;", " ");
      return html.trim();
    };
  }).filter('asDate', ($filter) => {
    'ngInject';
    return function(input, format) {
      if (input === null) {
        return "";
      }
      return $filter('date')(new Date(input), format ? format : 'dd/MM/yyyy HH:mm:ss');
    };
  }).filter('parseHtml', ($sce) => {
    'ngInject';
    return function(value) {
      if (!value) {
        return '';
      }
      return $sce.trustAsHtml(value);
    };
  })
  .filter('sliceString', () => {
    return function(value, wordwise, max, tail) {
      if (!value) {
        return '';
      }

      max = parseInt(max, 10);
      if (!max) {
        return value;
      }
      if (value.length <= max) {
        return value;
      }

      value = value.substr(0, max);
      if (wordwise) {
        const lastspace = value.lastIndexOf(' ');
        if (lastspace !== -1) {
          value = value.substr(0, lastspace);
        }
      }

      return value + (tail || '…');
    };
  })

  // .filter("purger", function() {
  //   return function(input) {
  //     return input.replace(/[^\w\s]/gi, "");
  //   }
  // })
  .filter('trim', () => {
    return function(text) {
      if (!angular.isString(text)) {
        return text;
      }
      const html = String(text).replace(/^\s+|\s+$/g, '');
      return html.trim();
    };
  })
  .filter('firstAndLast', () => {
    return function(text) {
      const words = text.split(" ");
      const name = words.shift() + " " + words.pop();
      return name;
    };
  })
  .filter("trust", ($sce) => {
    return function(htmlCode){
      return $sce.trustAsHtml(htmlCode);
    };
  }
        );

export default filtersModule;
