import PcMensagensMainModule from './pcMensagensMain';
import PcMensagensMainController from './pcMensagensMain.controller.js';
import PcMensagensMainComponent from './pcMensagensMain.component.js';
import PcMensagensMainTemplate from './pcMensagensMain.html';

describe('PcMensagensMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcMensagensMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcMensagensMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

    // controller specs

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcMensagensMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcMensagensMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcMensagensMainController);
    });
  });
});
