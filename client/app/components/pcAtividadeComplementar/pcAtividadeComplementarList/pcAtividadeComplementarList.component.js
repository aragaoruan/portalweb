import template from './pcAtividadeComplementarList.html';
import controller from './pcAtividadeComplementarList.controller';
import './pcAtividadeComplementarList.scss';

const pcAtividadeComplementarListComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcAtividadeComplementarListComponent;
