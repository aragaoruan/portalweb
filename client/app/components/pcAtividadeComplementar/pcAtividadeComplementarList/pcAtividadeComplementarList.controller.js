import loadingGif from '../../../../assets/img/loading.gif';

class PcAtividadeComplementarListController {
  constructor($state, $q, NotifyService, AtividadeComplementarService) {
    'ngInject';

    this.loadingGif = loadingGif;

    this.NotifyService = NotifyService;
    this.AtividadeComplementarService = AtividadeComplementarService;

    this.$state = $state;
    this.params = $state.params;
    this.atividades = [];
    this.nu_horasprojeto = null;
    this.nu_horasaluno = null;
    this.st_resumo = null;

    this.loading = true;

    const promises = [
      this.buscarHorasProjeto(),
      this.buscarAtividades()
    ];

    $q.all(promises).then(() => {
      this.loading = false;
    });
  }

  buscarHorasProjeto = () => {
    return this.AtividadeComplementarService.getHorasProjeto(this.params.idMatriculaAtiva).then((response) => {
      this.nu_horasprojeto = response.nu_horasatividades || 0;
      this.somarHorasAluno();
    });
  }

  buscarAtividades = () => {
    const params = {
      id_matricula: this.params.idMatriculaAtiva
    };

    return this.AtividadeComplementarService.getResource().getAll(params, (response) => {
      this.atividades = response;
      this.somarHorasAluno();
    }, (error) => {
      const message = error && error.data && error.data.message ? error.data.message : 'Ocorreu um erro ao buscar as atividades.';
      this.NotifyService.notify('error', '', message);
    });
  }

  somarHorasAluno = () => {
    let total = 0;

    if (this.atividades.length && this.nu_horasprojeto !== null) {
      this.atividades.forEach((item) => {
        total += parseFloat(item.nu_horasconvalidada || 0);
      });
    }

    this.nu_horasaluno = Math.min(total, this.nu_horasprojeto);
  }

  abrirForm = () => {
    if (this.nu_horasaluno >= this.nu_horasprojeto) {
      this.NotifyService.notify('error', '', 'Não é permitido postar uma nova atividade complementar porque você já atingiu o valor máximo necessário para conclusão.');
      return false;
    }

    this.$state.go('app.atividade-complementar-form', {idMatriculaAtiva: this.params.idMatriculaAtiva});
  }

}

export default PcAtividadeComplementarListController;
