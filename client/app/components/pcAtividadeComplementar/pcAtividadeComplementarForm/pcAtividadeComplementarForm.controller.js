import loadingGif from '../../../../assets/img/loading.gif';

class PcAtividadeComplementarFormController {
  constructor($state, $q, $localStorage, NotifyService, AtividadeComplementarService, TipoAtividadeComplementarService) {
    "ngInject";

    this.loadingGif = loadingGif;

    this.NotifyService = NotifyService;
    this.AtividadeComplementarService = AtividadeComplementarService;
    this.TipoAtividadeComplementarService = TipoAtividadeComplementarService;

    this.$state = $state;
    this.params = $state.params;
    this.matricula = angular.fromJson($localStorage['matricula.' + this.params.idMatriculaAtiva]);

    this.tiposAtividades = [];
    this.editModel = this.getModel();

    this.loading = true;

    const promises = [this.buscarTiposAtividades()];

    $q.all(promises).then(() => {
      this.loading = false;
    });
  }

  getModel = () => {
    return {
      id_matricula: this.params.idMatriculaAtiva,
      bl_ativo: true,
      anexo: null
    };
  }

  buscarTiposAtividades = () => {
    return this.TipoAtividadeComplementarService.getResource().getAll({}, (response) => {
      this.tiposAtividades = response;
    }, (error) => {
      const message = error && error.data && error.data.message ? error.data.message : 'Ocorreu um erro ao buscar os dados.';
      this.NotifyService.notify('error', '', message);
    });
  }

  voltarParaLista() {
    this.$state.go('app.atividade-complementar', {idMatriculaAtiva: this.params.idMatriculaAtiva});
  }

  salvarAtividadeComplementar = (model) => {
    if (!model.st_tituloatividade || !model.id_tipoatividade || !model.anexo) {
      this.NotifyService.notify('error', '', 'Campo obrigatório não preenchido.');
      return false;
    }

    model.id_entidadecadastro = this.matricula.id_entidade;

    this.loading = true;

    this.AtividadeComplementarService.enviarAtividadeComplementar(model)
      .success(() => {
        this.loading = false;
        this.NotifyService.notify('success', '', 'Atividade complementar cadastrada com sucesso.');
        this.voltarParaLista();
      })
      .error((response) => {
        this.loading = false;
        const message = response && response.data && response.data.message ? response.data.message : 'Ocorreu um erro ao salvar a atividade.';
        this.NotifyService.notify('error', '', message);
      });
  }

  validarArquivos = ($invalidFiles) => {
    if ($invalidFiles && $invalidFiles.length) {
      $invalidFiles.forEach(($file) => {
        if ($file.$error) {
          if ($file.$error === 'pattern') {
            this.NotifyService.notify('error', '', 'Formato inválido do arquivo. Os fortamos permitidos são: JPEG, PNG, DOC, DOCX e PDF.');
          }

          if ($file.$error === 'maxSize') {
            this.NotifyService.notify('error', '', 'O tamanho do anexo excede os 10mb permitido pelo sistema');
          }
        }
      });
    }
  }

}

export default PcAtividadeComplementarFormController;
