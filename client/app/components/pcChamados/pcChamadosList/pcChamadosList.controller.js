import loadingGif from '../../../../assets/img/loading.gif';

class PcChamadosListController {

  constructor(CONFIG, OcorrenciaService, $state, $window, NotifyService) {

    'ngInject';

    this.OcorrenciaService = OcorrenciaService;

    this.$state = $state;

    this.name = 'pcChamadosList';

    this.loadingGif = loadingGif;

    this.loading = false;

    this.chamados = [];

    //Mostra mensagem de erro caso ocorra algum erro na requisição de buscas as ocorrencias
    this.toaster = NotifyService;

    //Chama o método de buscar as ocorrencias
    this.buscarOcorrencias();

    // Variável para marcar o id ativo
    this.activeId;
  }


 /**
    Abre os detalhes de de uma ocorrencia (show)
 **/
  visualizarOcorrencia = (event, chamado) => {
    if (event){
      event.preventDefault();
    }
    this.activeId = chamado.id_ocorrencia;
    this.$state.go('app.chamados.show', {id_ocorrencia: chamado.id_ocorrencia}, {reload: false});
  }

  /**
  Busca as ocorrencias do usuário logado
  Listagem
  **/
  buscarOcorrencias = () => {
    this.loading = true;
    this.OcorrenciaService.getResource().getAll({ }, (response) => {
      this.loading = false;
      this.chamados = response;
      this.visualizarOcorrencia(null,response[0]);
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });

  }
}

export default PcChamadosListController;
