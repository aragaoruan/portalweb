import template from './pcChamadosList.html';
import controller from './pcChamadosList.controller';
import './pcChamadosList.scss';

const pcChamadosListComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcChamadosListComponent;
