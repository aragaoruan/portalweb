import loadingGif from "../../../../assets/img/loading.gif";
import templateModalReabrir from "./popupConfirmReopen.html";
import modalReabrirController from "./pcChamadosModalReabrir.controller";
import templateModalEncerrar from "./popupConfirmClose.html";
import modalEncerrarController from "./pcChamadosModalEncerrar.controller";
import templateModalInteracao from "./popupInteracao.html";
import modalInteracaoController from "./pcChamadosModalInteracao.controller";

//Modal Reabrir Chamado
//Modal Encerrar Chamado
//Modal Nova interação

class PcChamadosShowController {
  constructor(CONFIG, OcorrenciaService, $state, $window, NotifyService, InteracaoService, $uibModal, $scope, $localStorage) {
    'ngInject';

    this.storage = $localStorage;
    this.OcorrenciaService = OcorrenciaService;
    this.InteracaoService = InteracaoService;
    this.CONFIG = CONFIG;

    this.name = 'pcChamadosShow';

    this.vchamado = null;
    this.vinteracoes = [];

    this.loadingGif = loadingGif;

    this.loading = false;

    this.toaster = NotifyService;
    this.dialog = $uibModal;
    this.scope = $scope;

    const id_ocorrencia = $state.params.id_ocorrencia ? $state.params.id_ocorrencia : null;
    this.visualizarOcorrencia(id_ocorrencia);

  }

  visualizarOcorrencia = (id_ocorrencia) => {
    this.loading = true;

    this.OcorrenciaService.getResource().getOneBy({id: id_ocorrencia}
      , (response) => {
        this.vchamado = response;
        this.listaInteracoesOcorrencia(id_ocorrencia);
        this.loading = false;
      }, (error) => {
        this.loading = false;
        this.toaster.notify(error.data);
      });

  };


  /** Lista as interações de um chamado**/
  listaInteracoesOcorrencia = (id_ocorrencia) => {
    if (id_ocorrencia) {
      this.loading = true;
      this.InteracaoService.getResource().getAll({id_ocorrencia}, (response) => {
        this.loading = false;
        this.vinteracoes = response;
      }, (error) => {
        this.loading = false;
        this.toaster.notify(error.data);
      });
    }
  }

  /**
   Verifica se o usuario da ocorrencia é o mesmo logado
   Utilizada para verificar se pode reabrir um chamado
   **/
  isSameUserSession = (id_usuario) => {
    let answer = false;
    if (id_usuario) {
      if (this.storage.user_id === id_usuario) {
        answer = true;
      }
    }
    return answer;
  }

  /**
   Chama uma modal de confirmação caso o usuário queira reabrir um chamado
   **/
  reabrirChamado = (id_ocorrencia) => {
    this.reabrirModal = this.dialog.open({
      animation: true,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      template: templateModalReabrir,
      size: 'md',
      controller: modalReabrirController
      , controllerAs: 'modalReabrirCtrl'
      , resolve: {
        idOcorrencia () {
          return id_ocorrencia;
        }
      }
    });

    this.reabrirModal.result.then(() => {
      this.visualizarOcorrencia(id_ocorrencia);
    }
  );
  }

  /**
   Chama uma modal de confirmação caso o usuário queira encerrar um chamado
   **/
  encerrarChamado = (id_ocorrencia) => {
    this.encerrarModal =  this.dialog.open({
      animation: true,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      template: templateModalEncerrar,
      size: 'md',
      controller: modalEncerrarController
      , controllerAs: 'modalEncerrarCtrl'
      , resolve: {
        idOcorrencia () {
          return id_ocorrencia;
        }
      }
    });

    this.encerrarModal.result.then(() => {
      this.visualizarOcorrencia(id_ocorrencia);
    }

    );
  }

  /** Abre uma modal para a criação de uma nova interação para um chamado **/
  novaInteracao = (ocorrencia) => {
    this.dialog.open({
      animation: true,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      template: templateModalInteracao,
      size: 'md',
      controller: modalInteracaoController,
      controllerAs: 'modalInteracaoCtrl',
      resolve: {
        ocorrencia: () => {
          return ocorrencia;
        },
        interacoes: () => {
          return this.vinteracoes;
        }
      }
    });
  }

}

export default PcChamadosShowController;
