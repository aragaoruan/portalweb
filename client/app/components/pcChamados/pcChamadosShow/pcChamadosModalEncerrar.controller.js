import loadingGif from '../../../../assets/img/loading.gif';

// Constoller para ação de reabrir um chamado
class PcChamadosModalEncerrarController {
  constructor(CONFIG, OcorrenciaService, $state, $window, NotifyService, $uibModal, $uibModalInstance, idOcorrencia, $localStorage, MensagemPadrao) {
    'ngInject';

    this.OcorrenciaService = OcorrenciaService;
    this.name = 'pcChamadosModalEncerrar';
    this.loadingGif = loadingGif;
    this.loading = false;
    this.$state = $state;
    this.storage = $localStorage;
    this.toaster = NotifyService;
    this.modalEncerrar = $uibModalInstance;
    this.id_ocorrencia = idOcorrencia;
    this.MensagemPadrao = MensagemPadrao;
  }

  //Fecha Modal de confirmação de encerramento de chamado
  closeModalEncerrarChamado = () => {
    this.modalEncerrar.close();
  }

  //Reabre um chamado e atualiza a pagina para mostrar a nova evolução
  saveEncerrarChamado = (idOcorrencia) => {

    this.OcorrenciaService.encerrarChamado(idOcorrencia).then(() => {
      this.loading = true;
      this.toaster.notify('success','', this.storage.user_shortname + this.MensagemPadrao.OCORRENCIA_ENCERRADA_SUCESSO);
      this.modalEncerrar.close();
      this.$state.go('app.chamados.show', {id_ocorrencia: idOcorrencia});
    }, (error) => {
      this.toaster.notify(error.data);
    }).finally(() => {
      this.loading = false;
    });
  }
}

export default PcChamadosModalEncerrarController;
