import loadingGif from "../../../../assets/img/loading.gif";

// Constoller para ação  (modal) de criar uma nova interação
class PcChamadosModalInteracaoController {
  constructor(CONFIG, InteracaoService, $state, $window, NotifyService, $scope, $uibModalInstance, ocorrencia, interacoes, Upload, MensagemPadrao, $localStorage) {
    'ngInject';

    this.InteracaoService = InteracaoService;
    this.name = 'pcChamadosModalInteracao';
    this.loadingGif = loadingGif;
    this.loading = false;
    this.$scope = $scope;
    this.$state = $state;
    this.toaster = NotifyService;
    this.modalNovaInteracao = $uibModalInstance;
    this.ocorrencia = ocorrencia;
    this.st_tramite = '';
    this.interacoes = interacoes;
    this.Upload = Upload;
    this.MensagemPadrao = MensagemPadrao;
    this.storage =  $localStorage;

    this.arquivo_ocorrencia = null;
    this.enviandoArquivo = false;

  }

  //Fecha Modal
  close = () => {
    this.modalNovaInteracao.close();
  }

  /**
   * Salva uma nova interação com anexo.
   * Recebe os dados a serem salvos e envia para Service, efetuando o cadastro da interaçao
   *
   * @returns {boolean}
   */
  save = () => {

    if (!this.st_tramite) {
      this.toaster.notify('error', 'Preençha o campo com sua interação.');
      return false;
    }

    const tramite = {
      st_tramite: this.st_tramite,
      id_entidade: this.ocorrencia.id_entidade,
      id_evolucao: this.ocorrencia.id_evolucao,
      id_usuariointeressado: this.ocorrencia.id_usuariointeressado,
      bl_visivel: true,
      file: this.files,
      id_ocorrencia: this.ocorrencia.id_ocorrencia
    };

    this.InteracaoService.novaInteracao(tramite).success((response) => {
      this.toaster.notify('success', '', this.storage.user_shortname + this.MensagemPadrao.INTERACAO_CADASTRADA_SUCESSO);
      this.modalNovaInteracao.close() ;
      this.interacoes.unshift(response);
    }).error((error) => {
      this.toaster.notify(error);
    });
  };

  /**
   * Recebe um arquivo e faz as verificaçoes necessarias para permitir o upload
   * caso fique fora do permitido limpa o campo para nao enviar o arquivo
   */
  verificacaoArquivo = () => {
    if (this.files) {
      var formatosPermitidos = ["application/msword", "application/pdf", "image/png", "image/jpg", "image/jpeg", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"];

      if (formatosPermitidos.indexOf(this.files.type) === -1) {
        this.toaster.notify('error',  this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_FORA_DO_FORMATO_PERMITIDO);
        this.files = {};
      }

      if (this.files.size > 10000000) {
        this.toaster.notify('error',  this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_TAMANHO);
        this.files = {};
      }
    }
  }
}

export default PcChamadosModalInteracaoController;
