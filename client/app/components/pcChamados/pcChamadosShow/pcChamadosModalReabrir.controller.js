import loadingGif from '../../../../assets/img/loading.gif';

// Constoller para ação de reabrir um chamado
class PcChamadosModalReabrirController {
  constructor(CONFIG, OcorrenciaService, $state, $window, NotifyService, $uibModal, $uibModalInstance, idOcorrencia, $localStorage, MensagemPadrao) {
    'ngInject';

    this.OcorrenciaService = OcorrenciaService;
    this.name = 'pcChamadosModalReabrir';
    this.loadingGif = loadingGif;
    this.loading = false;
    this.$state = $state;
    this.toaster = NotifyService;
    this.modalReabrir = $uibModalInstance;
    this.id_ocorrencia = idOcorrencia;
    this.MensagemPadrao = MensagemPadrao;
    this.storage = $localStorage;
  }

  //Fecha Modal de reabrir chamado
  closeModalReabrirChamado = () => {
    this.modalReabrir.close();
  }

  //Reabre um chamado e atualiza a pagina para mostrar a nova evolução
  saveReabrirChamado = (idOcorrencia) => {

    this.OcorrenciaService.reabrirChamado(idOcorrencia).then(() => {
      this.loading = true;
      this.toaster.notify('success','', this.storage.user_shortname + this.MensagemPadrao.REABRIR_OCORRENCIA_SUCESSO);
      this.modalReabrir.close();
      this.$state.go('app.chamados.show', {id_ocorrencia: idOcorrencia});
    }, (error) => {
      this.toaster.notify(error.data);
    }).finally(() => {
      this.loading = false;
    });
  }
}

export default PcChamadosModalReabrirController;
