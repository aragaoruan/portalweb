import template from './pcChamadosMain.html';
import controller from './pcChamadosMain.controller';
import './pcChamadosMain.scss';

const pcChamadosMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcChamadosMainComponent;
