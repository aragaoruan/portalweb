import template from './pcGradeNotasForm.html';
import controller from './pcGradeNotasForm.controller';
import './pcGradeNotasForm.scss';

const pcGradeNotasFormComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcGradeNotasFormComponent;
