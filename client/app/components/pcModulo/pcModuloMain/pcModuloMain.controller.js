class PcModuloMainController {
  constructor(MoodleService, $state, $q, $sce) {
    'ngInject';

    this.MoodleService = MoodleService;
    this.$q = $q;
    this.$sce = $sce;


    this.idMatricula = $state.params.idMatriculaAtiva; // Matrícula atual do Aluno
    this.idModulo = $state.params.idModulo; // Id do Módulo no Moodle
    this.idCurso = $state.params.idCurso; // Id do Curso no Moodle
    this.idSaladeaula = $state.params.idSaladeaula; // Sala de aula no G2
    this.modulo = [];
    this.modulourl = null;
    this.fullscreen = false;

    if (this.idMatricula && this.idModulo && this.idCurso && this.idSaladeaula) {
      this.CourseContents(this.idMatricula, this.idCurso, this.idModulo, this.idSaladeaula).then(
        (success) => {
          this.modulo = success;

          if (success.url) {
            console.log(success.loginurl);
            this.modulourl = $sce.trustAsResourceUrl(success.loginurl + '&wantsurl=' + success.url);
          }
          else {
            console.log('else');

            this.modulourl = $sce.trustAsResourceUrl(success.loginurl);
          }

        },
        () => {

          // falta definir o que fazer ao receber um erro
        }
      );
    }
  }

  /**
   * Retorna o conteúdo do curso do Moodle e verifica se é igual ao solicitado
   * @param idMatricula - Matrícula atual do Aluno
   * @param idCurso - Id do Módulo no Moodle
   * @param idModulo - Id do Curso no Moodle
   * @param idSaladeaula - Id da sala de aula no G2
   * @returns {*}
   * @constructor
   */
  CourseContents = (idMatricula, idCurso, idModulo, idSaladeaula) => {

    return this.$q((resolve, reject) => {

      // Passando o clearCache como true para resolver problema de usuario acessando em dois navegadores
      // @todo: implementar lógica de verificação
      this.MoodleService.getTokenAcesso(idMatricula, idSaladeaula,true).then((sucess) => {
        this.MoodleService.getCourseContents(idMatricula, idCurso, idSaladeaula).then((contents) => {
          angular.forEach(contents, (value) => {
            angular.forEach(value.modules, (mvalue) => {
              if (parseInt(mvalue.id) === parseInt(idModulo)) {
                mvalue.loginurl = sucess.data.loginurl;
                resolve(mvalue);
              }
            });
          });
        }, (error) => {
          reject(error);
        });
      }, (error) => {
        reject(error);
      });

    });


  }

}

export default PcModuloMainController;
