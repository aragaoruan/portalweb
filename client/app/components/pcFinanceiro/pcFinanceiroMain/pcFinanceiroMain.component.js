import template from './pcFinanceiroMain.html';
import controller from './pcFinanceiroMain.controller';
import './pcFinanceiroMain.scss';

const pcFinanceiroMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcFinanceiroMainComponent;
