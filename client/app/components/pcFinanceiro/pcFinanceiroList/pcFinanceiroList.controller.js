
import loadingGif from '../../../../assets/img/loading.gif';

class PcFinanceiroListController {
  constructor(FinanceiroService, $state, NotifyService,$rootScope) {
    'ngInject';
    this.name = 'pcFinanceiroList';
    this.$state = $state;
    this.$scope = $rootScope;
    this.toaster = NotifyService;
    this.lancamentos = [];
    this.lancamentosPagos = [];
    this.lancamentosAtrasados = [];
    this.lancamentosPendentes = [];

    this.status = ['pendentes','atrasados','pagos'];
    angular.forEach(this.status, (statusName) => {
      this.lancamentos[statusName] = [];
    });

    this.loadingGif = loadingGif;

    this.loading = false;

    this.FinanceiroService = FinanceiroService;

    angular.forEach(this.parcelas, (lancamento) => {
      switch (lancamento.id_statuslancamento) {
      case 1:
        this.lancamentosPagos.push(lancamento);
        this.lancamentos[this.status[2]].push(this.parcelas);
        break;
      case 3:
        this.lancamentosAtrasados.push(lancamento);
        this.lancamentos[this.status[1]].push(this.parcelas);
        break;
      case 2:
        this.lancamentosPendentes.push(lancamento);
        this.lancamentos[this.status[0]].push(this.parcelas);
        break;
      }
    });
  }

  visualizarLancamento = (event, lancamento) => {
    this.doRefresh(lancamento.id_lancamento);
    event.preventDefault();
  }

}

export default PcFinanceiroListController;
