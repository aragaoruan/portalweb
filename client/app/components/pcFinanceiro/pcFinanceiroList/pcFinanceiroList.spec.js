import PcFinanceiroListModule from "./pcFinanceiroList";
import PcFinanceiroListController from "./pcFinanceiroList.controller";
import PcFinanceiroListComponent from "./pcFinanceiroList.component";
import PcFinanceiroListTemplate from "./pcFinanceiroList.html";

describe("PcFinanceiroList", () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcFinanceiroListModule.name));
  beforeEach(
    inject((_$rootScope_) => {
      $rootScope = _$rootScope_;
      makeController = () => {
        return new PcFinanceiroListController();
      };
    })
  );

  describe("Module", () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe("Template", () => {

  });

  describe("Component", () => {
    // component/directive specs
    const component = PcFinanceiroListComponent;

    it("includes the intended template", () => {
      expect(component.template).to.equal(PcFinanceiroListTemplate);
    });

    it("uses `controllerAs` syntax", () => {
      expect(component).to.have.property("controllerAs");
    });

    it("invokes the right controller", () => {
      expect(component.controller).to.equal(PcFinanceiroListController);
    });
  });
});
