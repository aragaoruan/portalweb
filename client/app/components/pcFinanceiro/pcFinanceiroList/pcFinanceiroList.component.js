import template from './pcFinanceiroList.html';
import controller from './pcFinanceiroList.controller';
import './pcFinanceiroList.scss';

const pcFinanceiroListComponent = {
  restrict: 'E',
  bindings: {
    doRefresh: '=',
    parcelas: '='
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcFinanceiroListComponent;
