import loadingGif from "../../../../assets/img/loading.gif";

class PcFinanceiroFormController {
  constructor(FinanceiroService, $state, NotifyService, CONFIG, MeioPagamentoConstante, StatusLancamentoConstante, DownloadService,$localStorage,$window,$uibModal) {
    'ngInject';
    this.storage =$localStorage;
    this.name = 'pcFinanceiroForm';
    this.loadingGif = loadingGif;
    this.toaster = NotifyService;
    this.lancamentos = null;
    this.loaded = false;
    this.CONFIG = CONFIG;
    this.$state = $state;
    this.loading = false;
    this.boleto = null;
    this.id_lancamento = this.id_lancamento ? this.id_lancamento : ($state.params.id_lancamento ? $state.params.id_lancamento : null);
    this.lancamento = null;
    this.FinanceiroService = FinanceiroService;
    this.MeioPagamentoConstant = MeioPagamentoConstante;
    this.StatusLancamentoConstante = StatusLancamentoConstante;
    this.DownloadService = DownloadService;
    this.$window = $window;
    this.dialog = $uibModal;
    this.buscarLancamentos();
    this.labelBtGerarBoleto = 'Gerar Boleto';
    this.showMsgPopup = null;
  }

  /**
   * Retorna os lançamentos para que serem exibidos
   */


  buscarLancamentos = () => {
    this.loading = true;

    this.FinanceiroService.getVendasUser().then((response) => {
      this.lancamentos = response;
    }, (error) => {
      this.toaster.notify(error.data);
    }).finally(() => {
      this.loading = false;

      //Filtra os lancamentos nao pagos ainda
      this.naoPago = this.lancamentos.filter((lancamento) => lancamento.id_statuslancamento !== this.StatusLancamentoConstante.PAGO);

      //verifica se encontrou lancamentos que não foram pagos
      if (this.naoPago.length) {
        //Ordena pelo vencimento
        this.naoPago.sort((a, b) => {
          return new Date(a.dt_vencimento).getTime() - new Date(b.dt_vencimento).getTime();
        });

        // Carrega o lancamento nao pago mais antigo
        this.doRefresh(this.naoPago[0].id_lancamento);
      }
      else {
        //notifica que não foi encontrado lançamentos pendentes
        this.toaster.notify("warning","Nenhum lançamento pendente.");
      }
    });

  }

// Funcao de carregar um registro. Tambem passada no bind para o list
  doRefresh = (id_lancamento) => {
    this.id_lancamento = id_lancamento;
    this.getDadosLancamento(id_lancamento);
    this.$state.go('app.financeiro.form', {id_lancamento}, {reload: false});
  }

  /**
   * Retorna os dados do boleto
   * @param id_lancamento
   */
  getDadosLancamento = (id_lancamento) => {
    this.loading = true;
    this.labelBtGerarBoleto = 'Gerar Boleto';
    this.FinanceiroService.getOneById(id_lancamento).then((response) => {
      this.lancamento = response;

      //verifica se o meio de pagamento é boleto.
      if (this.lancamento.id_meiopagamento === this.MeioPagamentoConstant.BOLETO) {
        this.boleto = response;
        if (this.boleto.st_codigobarras && this.boleto.st_situacao === 'Pendente') {
          this.labelBtGerarBoleto = 'Emitir Boleto';
        }

        // Com o boleto registrado, fica sendo obrigatória a interação do aluno para geração do boleto. Sendo assim, só mostramos aqui boletos já gerados.
        // this.getDadosBoleto(this.lancamento.id_lancamento);

      }
    }, (error) => {
      this.toaster.notify(error.data);
    }).finally(() => {
      this.loading = false;
    });
  }

  emitirDadosBoleto = (id_lancamento, funcao = 'getbarcode', id_venda) => {

    this.questionModal = this.dialog.open({
      animation: true,
      component: 'pcModalConfirm',
      resolve: {
        data: () => {
          return {
            title: 'Emissão de Boleto',
            question: 'Deseja realmente solicitar o boleto?',
            okText: 'Confirmar',
            cancelText: 'Cancelar'
          };
        }
      }
    }).result.then((result) => {
      if (result) {
        this.getDadosBoleto(id_lancamento, funcao, id_venda);
      }
    });

  }

  /**
   * Retorna os dados do lançamento solicitado dia id_lançamento
   * @param id_lancamento
   * @param funcao
   */
  getDadosBoleto = (id_lancamento, funcao = 'getbarcode', id_venda, primeiraVez = true) => {

    this.loading = true;
    this.showMsgPopup = null;

    if (funcao !== 'emitirboleto') {

      this.FinanceiroService.getDadosBoleto(id_lancamento, funcao).then((response) => {
        this.boleto = response;
      }, (error) => {
        this.toaster.notify(error.data);
      }).finally(() => {
        this.loading = false;
      });

    }
    else {

      this.FinanceiroService.getUrlBoleto(id_lancamento, id_venda).then((response) => {

        if (!response.st_codigobarras && primeiraVez) {
          setTimeout(() => {
            this.getDadosBoleto(id_lancamento, funcao, false);
          }, 7000);
        }
        else {

          this.questionModal = this.dialog.open({
            animation: true,
            component: 'pcModalConfirm',
            resolve: {
              data: () => {
                return {
                  title: 'Emissão de Boleto',
                  question: response.st_mensagem,
                  okText: 'Entendi',
                  showCancelButton: false
                };
              }
            }
          });

          if (response.st_codigobarras) {
            this.boleto.st_codigobarras = response.st_codigobarras;
          }

          if (response.st_urlboleto) {
            console.log(response);
            const popupWin = this.$window.open(`${this.CONFIG.urlG2}/loja/recebimento/?venda=${id_venda}&lancamento=${id_lancamento}`, '_blank', 'width=700,height=600');
            if (!popupWin) {
              this.showMsgPopup = 'Você precisa liberar os popups para emitir boletos';
            }
          }

          this.getDadosLancamento(id_lancamento);

        }

      }, () => {
        this.loading = false;
        this.toaster.notify('error', '', 'Não foi possívell emitir o boleto!');
      }).finally(() => {
        if (!primeiraVez) {
          this.loading = false;
        }
      });
    }
  }

  /**
   * Efetua o download do recibo no formato PDF
   * @param lancamento
   */
  getRecibo = (lancamento) => {
    if (!lancamento.st_link) {
      this.toaster.notify('error', '', 'Recibo indisponível');
    }
    else {
      const popupWin = this.$window.open(this.CONFIG.urlG2 + lancamento.st_link.replace('=html', '=pdf'));
      if (!popupWin) {
        this.showMsgPopup = 'Você precisa liberar os popups para emitir boletos';
      }
    }
  }
}

export default PcFinanceiroFormController;
