import PcMeusDadosMainModule from './pcMeusDadosMain';
import PcMeusDadosMainController from './pcMeusDadosMain.controller';
import PcMeusDadosMainComponent from './pcMeusDadosMain.component';
import PcMeusDadosMainTemplate from './pcMeusDadosMain.html';

describe('PcMeusDadosMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcMeusDadosMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcMeusDadosMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

    // controller specs
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcMeusDadosMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcMeusDadosMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcMeusDadosMainController);
    });
  });
});
