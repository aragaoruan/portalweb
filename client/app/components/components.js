
// Padrão definido em http://confluence.unyleya.com.br/pages/viewpage.action?spaceKey=DEV&title=Desenvolvimento+Novo+Portal
import angular from "angular";
import Layout from "./pcLayout/pcLayout";
import Login from "./pcLogin/pcLoginMain/pcLoginMain";
import LoginForm from "./pcLogin/pcLoginForm/pcLoginForm";
import LoginDireto from "./pcLogin/pcLoginDireto/pcLoginDireto";
import LoginSelecionaCursoForm from "./pcLogin/pcLoginSelecionaCursoForm/pcLoginSelecionaCursoForm";

// Mensagens
import MensagensMain from "./pcMensagens/pcMensagensMain/pcMensagensMain";
import MensagensList from "./pcMensagens/pcMensagensList/pcMensagensList";
import MensagensShow from "./pcMensagens/pcMensagensShow/pcMensagensShow";

// Visão geral do curso
import Curso from "./pcCurso/pcCursoMain/pcCursoMain";
import CursoDisciplinaList from "./pcCurso/pcCursoDisciplinaList/pcCursoDisciplinaList";

// Financeiro
import FinanceiroMain from "./pcFinanceiro/pcFinanceiroMain/pcFinanceiroMain";
import Financeirolist from "./pcFinanceiro/pcFinanceiroList/pcFinanceiroList";
import FinanceiroForm from "./pcFinanceiro/pcFinanceiroForm/pcFinanceiroForm";

//Meus dados
import MeusDadosMain from "./pcMeusDados/pcMeusDadosMain/pcMeusDadosMain";
import MeusDadosForm from "./pcMeusDados/pcMeusDadosForm/pcMeusDadosForm";

//Ocorrencias [Menu Chamados]/ [Title]Atendimento
import ChamadosMain from "./pcChamados/pcChamadosMain/pcChamadosMain";
import ChamadosList from "./pcChamados/pcChamadosList/pcChamadosList";
import ChamadosShow from "./pcChamados/pcChamadosShow/pcChamadosShow";

//Promoções
import PromocoesMain from "./pcPromocoes/pcPromocoesMain/pcPromocoesMain";
import PromocoesList from "./pcPromocoes/pcPromocoesList/pcPromocoesList";
import PromocoesShow from "./pcPromocoes/pcPromocoesShow/pcPromocoesShow";

//Modulo, uma das telas do Moodle
import ModuloMain from './pcModulo/pcModuloMain/pcModuloMain';

// Atividade Complementar
import AtividadeComplementarMain from './pcAtividadeComplementar/pcAtividadeComplementarMain/pcAtividadeComplementarMain';
import AtividadeComplementarList from './pcAtividadeComplementar/pcAtividadeComplementarList/pcAtividadeComplementarList';
import AtividadeComplementarForm from './pcAtividadeComplementar/pcAtividadeComplementarForm/pcAtividadeComplementarForm';

//Disciplina
import DisciplinaMain from './pcDisciplina/pcDisciplinaMain/pcDisciplinaMain';

//Grade Notas
import GradeNotasDetalhamentoMain from './pcGradeNotasDetalhamento/pcGradeNotasDetalhamentoMain/pcGradeNotasDetalhamentoMain';
import GradeNotasMain from './pcGradeNotas/pcGradeNotasMain/pcGradeNotasMain';
import GradeNotasForm from './pcGradeNotas/pcGradeNotasForm/pcGradeNotasForm';

//Meus Documentos
import MeusDocumentosMain from './pcMeusDocumentos/pcMeusDocumentosMain/pcMeusDocumentosMain';

//Biblioteca Virtual Pearson
import PearsonMain from './pcPearson/pcPearsonMain/pcPearsonMain';

//Biblioteca Polo Pergamum
import PergamumMain from './pcPergamum/pcPergamumMain/pcPergamumMain';

import PcModalConfirm from './pcModal/pcModalConfirm/pcModalConfirm';

export default angular.module('app.components', [
  Login.name,
  LoginForm.name,
  LoginDireto.name,
  LoginSelecionaCursoForm.name,
  MensagensMain.name,
  MensagensList.name,
  MensagensShow.name,
  Curso.name,
  CursoDisciplinaList.name,
  FinanceiroMain.name,
  Financeirolist.name,
  FinanceiroForm.name,
  Layout.name,
  MeusDadosMain.name,
  MeusDadosForm.name,
  ChamadosMain.name,
  ChamadosList.name,
  ChamadosShow.name,

  PromocoesMain.name,
  PromocoesList.name,
  PromocoesShow.name,


  ModuloMain.name,
  AtividadeComplementarMain.name,
  AtividadeComplementarList.name,
  AtividadeComplementarForm.name,
  GradeNotasDetalhamentoMain.name,
  GradeNotasMain.name,
  GradeNotasForm.name,
  DisciplinaMain.name,
  PearsonMain.name,
  PergamumMain.name,

  MeusDocumentosMain.name,
  PcModalConfirm.name
]);
