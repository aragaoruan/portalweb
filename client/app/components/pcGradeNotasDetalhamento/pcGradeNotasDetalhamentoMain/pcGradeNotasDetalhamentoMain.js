/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcGradeNotasDetalhamentoMainComponent from './pcGradeNotasDetalhamentoMain.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcGradeNotasDetalhamentoMainModule = angular.module('pcGradeNotasDetalhamentoMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .config(($stateProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definindo rotas para componente.
    $stateProvider
      .state('app.grade-notas-detalhamento', {
        url: '/:idMatriculaAtiva/grade-notas/:idDisciplina/detalhamento',
        views: {
          content: {
            template: '<pc-grade-notas-detalhamento-main></pc-grade-notas-detalhamento-main>'
          }
        }
      });
  })

  .component('pcGradeNotasDetalhamentoMain', pcGradeNotasDetalhamentoMainComponent);

export default pcGradeNotasDetalhamentoMainModule;
