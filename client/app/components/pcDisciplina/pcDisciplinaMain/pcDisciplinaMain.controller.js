class PcDisciplinaMainController {
  constructor(DisciplinaAlunoService, CursoAlunoService, NotifyService, $state) {
    'ngInject';
    this.DisciplinaAlunoService = DisciplinaAlunoService;
    this.NotifyService = NotifyService;

    //pega o id da sala de aula do parametro
    this.idSalaDeAula = $state.params.idSalaDeAula;

    //cria uma variavel no escopo para a matricula ativa
    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);

    //verifica se existe a matricula e a sala de aula
    if (this.MatriculaAtiva.id_matricula && this.idSalaDeAula) {
      //busca os dados da sala de aula
      this.DisciplinaAlunoService
        .retornarDisciplina(
          this.idSalaDeAula,
          this.MatriculaAtiva.id_matricula,
          this.MatriculaAtiva.id_entidade,
          1,
          true
        ).then((success) => {
          this.sala = success;
        }, (error) => {
          NotifyService.notify('error', error);
        });
    }

  }

}


export default PcDisciplinaMainController;
