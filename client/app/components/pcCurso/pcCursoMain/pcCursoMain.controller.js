import templateModal from "../../../widgets/primeiro-acesso/pwPrimeiroAcesso/pwPrimeiroAcesso.html";
import modalPrimeiroAcessoController from "../../../widgets/primeiro-acesso/pwPrimeiroAcesso/pwPrimeiroAcesso.controller";

class PcCursoMainController {
  constructor(CursoAlunoService, $state, $uibModal, $scope, $rootScope) {
    'ngInject';
    this.name = 'pcCursoMain';
    this.$rootScope = $rootScope;
    this.modal = $uibModal;
    this.CursoAlunoService = CursoAlunoService;

    //variavel do scope para alterar a visualizacao dos states com 3 colunas ou duas colunas e duas linhas
    this.toggleState = false;

    //cria uma variavel no escopo para a matricula ativa
    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);

    //verifica se é pra redirecionar o usuário para a tela de primeiro acesso
    if (this.MatriculaAtiva.bl_primeiroacesso) {
      this.modal.open({
        template: templateModal,
        controller: modalPrimeiroAcessoController,
        controllerAs: 'pwpa',
        size: 'lg',
        scope: $scope,
        resolve: {
          primeiroacesso: () => {
            return this.MatriculaAtiva;
          }
        }
      });
    }

    /**
     * Evento que atualiza Storage do Curso Ativo
     * @constructor
     */
    this.$rootScope.$on('atualizaStorage', () => {
      this.CursoAlunoService.removeStorageCursoAtivo(this.MatriculaAtiva.id_matricula);
      this.CursoAlunoService.verificaMatriculaAluno(this.MatriculaAtiva.id_matricula);
      this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);
    });

  }
}

export default PcCursoMainController;
