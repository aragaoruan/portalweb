import PcCursoMainModule from './pcCursoMain';
import PcCursoMainController from './pcCursoMain.controller.js';
import PcCursoMainComponent from './pcCursoMain.component.js';
import PcCursoMainTemplate from './pcCursoMain.html';

describe('PcCursoMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcCursoMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcCursoMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcCursoMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcCursoMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcCursoMainController);
    });
  });
});
