/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from "angular";
import uiRouter from "angular-ui-router";
import pcCursoMainComponent from "./pcCursoMain.component.js";
import Services from "../../../services/Services";

//Import modulo de rotas angular
//Import componente do modulo
//Import do modulo de services do portal

//Modulo principal do componente
const pcCursoMainModule = angular.module('pcCursoMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])
  .config(($stateProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definicao de rotas para o componente
    $stateProvider
      .state('app.curso', {
        url: '/:idMatriculaAtiva/curso',
        views: {
          content: {
            template: '<pc-curso-main></pc-curso-main>'
          }
        }, onEnter (AuthService, $state) {
          const params = $state.params;

              //grava log de acesso
          AuthService.gravaLogAcesso($state.current.name, params);
        }
      });
  })

  .component('pcCursoMain', pcCursoMainComponent);

export default pcCursoMainModule;
