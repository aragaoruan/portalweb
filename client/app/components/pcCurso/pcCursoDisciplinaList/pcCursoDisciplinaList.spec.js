import PcCursoDisciplinaListModule from './pcCursoDisciplinaList';
import PcCursoDisciplinaListController from './pcCursoDisciplinaList.controller';
import PcCursoDisciplinaListComponent from './pcCursoDisciplinaList.component';
import PcCursoDisciplinaListTemplate from './pcCursoDisciplinaList.html';

describe('PcCursoDisciplinaList', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcCursoDisciplinaListModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcCursoDisciplinaListController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcCursoDisciplinaListComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcCursoDisciplinaListTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcCursoDisciplinaListController);
    });
  });
});
