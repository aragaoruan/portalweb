/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcCursoDisciplinaListComponent from './pcCursoDisciplinaList.component';

//Import do modulo de services do portal
import Services from '../../../services/Services';

//Modulo principal do componente
const pcCursoDisciplinaListModule = angular.module('pcCursoDisciplinaList', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])
  .config(($stateProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definicao de rotas para o componente
    $stateProvider
      .state('app.curso.disciplina', {
        url: '/disciplina/:id_disciplina',
        views: {
          'curso-disciplina-view': {
            template: '<pc-curso-disciplina-list></pc-curso-disciplina-list>'
          }
        }
      });
  })
.component('pcCursoDisciplinaList', pcCursoDisciplinaListComponent);

export default pcCursoDisciplinaListModule;
