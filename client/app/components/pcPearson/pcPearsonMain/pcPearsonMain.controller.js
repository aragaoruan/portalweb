class PcPearsonMainController {
  constructor(NotifyService, PearsonService, $state, $window, $sce) {
    'ngInject';

    this.NotifyService = NotifyService;
    this.PearsonService = PearsonService;

    this.params = $state.params;
    this.$window = $window;
    this.$sce = $sce;

    this.status = 'Buscando bilbioteca virtual...';
    this.url = null;
    this.integracao = {};

    this.buscarIntegracao();
  }

  buscarIntegracao = () => {
    const params = {
      id_matricula: this.params.idMatriculaAtiva
    };

    this.PearsonService.getResource().getOneBy(params, (response) => {
      this.integracao = response;
      this.integracao.st_caminho = this.$sce.trustAsResourceUrl(this.integracao.st_caminho);

      this.status = 1;
      this.url = `${response.st_caminho}?login=${response.st_usuario}&token=${response.st_login}`;

      const win = this.$window.open(this.url, '_blank');

      if (win) {
        win.focus();
      }
      else {
        this.NotifyService.notify('warning', '', 'É necessário permitir que o navegador abra uma nova aba.');
      }
    }, (error) => {
      this.status = error.data.message;
      this.NotifyService.notify('error', '', this.status);
    });
  }
}

export default PcPearsonMainController;
