import PcLoginMainModule from './pcLoginMain';
import PcLoginMainController from './pcLoginMain.controller.js';
import PcLoginMainComponent from './pcLoginMain.component.js';
import PcLoginMainTemplate from './pcLoginMain.html';

describe('PcLoginMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcLoginMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcLoginMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Component', () => {
      // component/directive specs
    const component = PcLoginMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcLoginMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcLoginMainController);
    });
  });
});
