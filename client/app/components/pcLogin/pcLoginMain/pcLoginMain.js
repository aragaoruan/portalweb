/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';
import angularLadda from 'angular-ladda';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcLoginMainComponent from './pcLoginMain.component.js';

//Import do modulo de services do portal
import Services from '../../../services/Services';


import ngAnimate from 'angular-animate';


//Modulo principal do componente
const pcLoginMainModule = angular.module('pcLoginMain', [
  uiRouter,
  angularLadda,
  ngAnimate,
  Services.name //Injecao modulo services do portal
])
    .config(($stateProvider, $urlRouterProvider, laddaProvider) => {
      "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services


      laddaProvider.setOption({

            /* optional */
        style: 'expand-left',
        spinnerSize: 20,
        spinnerColor: '#ffffff'
      });

      $urlRouterProvider.otherwise('/login');

        //Definicao de rotas para o componente
      $stateProvider

            .state('login', {
              url: '/login',
              template: '<pc-login-main></pc-login-main>'
            })
            .state('logout', {
              url: '/logout',
              onEnter (AuthService, $state) {
                const params = $state.params;

                    //grava log de acesso
                AuthService.gravaLogAcesso($state.current.name, params);

                AuthService.destroyUserCredentials();
                $state.go('login');

                // este reload é importante para garantir que o bot desapareça da tela.
                location.reload();
              }
            })
        ;
    })
    .component('pcLoginMain', pcLoginMainComponent);

export default pcLoginMainModule;
