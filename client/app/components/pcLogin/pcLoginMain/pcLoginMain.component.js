import template from './pcLoginMain.html';
import controller from './pcLoginMain.controller.js';
import './pcLoginMain.scss';

//Imagens estáticas


const pcLoginMainComponent = {
  restrict: 'E',
  bindings: {
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcLoginMainComponent;
