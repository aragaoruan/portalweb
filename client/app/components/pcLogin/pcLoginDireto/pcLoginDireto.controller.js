import logo from '../../../../assets/img/logo.png';
class PcLoginDiretoController {
  constructor($state, AuthService, NotifyService) {
    'ngInject';

    this.logoUrl = logo;
    this.state = $state;
    this.AuthService = AuthService;
    this.name = 'vm';

    this.showErrorLogin = false;

    this.blGravaLog = this.state.params.blGravaLog
      , this.idUsuarioOriginal = this.state.params.idUsuarioOriginal
      , this.idEntidade = this.state.params.idEntidade
      , this.idUsuario = this.state.params.idUsuario
      , this.stUrlAcesso = this.state.params.stUrlAcesso;

    if (this.idUsuarioOriginal && this.idEntidade && this.idUsuario && this.stUrlAcesso) {

      this.AuthService.autenticacaoDireta(this.blGravaLog
        , this.idUsuarioOriginal
        , this.idEntidade
        , this.idUsuario
        , this.stUrlAcesso).then(
        (response) => {

          if (response) {

            if (response.type === 'success') {
              $state.go('login.selecionar-curso');
            }
          }
          else {
            NotifyService.notify('error', '', this.MensagemPadrao.ERRO_REQUISICAO);
            this.showErrorLogin = true;
          }

        }
        , (error) => {
          NotifyService.notify(error.data);
          this.showErrorLogin = true;
        });
    }
    else {
      this.showErrorLogin = true;
    }
  }
}

export default PcLoginDiretoController;
