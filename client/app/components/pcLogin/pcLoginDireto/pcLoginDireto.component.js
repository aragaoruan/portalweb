import template from './pcLoginDireto.html';
import controller from './pcLoginDireto.controller';
import './pcLoginDireto.scss';

const pcLoginDiretoComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcLoginDiretoComponent;
