import logo from '../../../../assets/img/logo-1.png';
const moment = require('moment');

class PcLoginSelecionaCursoFormController {
  constructor(CursoAlunoService, AuthService, $state, NotifyService, EvolucaoMatriculaConstante,
              MensagensInfoEvolucaoConstante, CertificacaoService) {
    'ngInject';
    this.name = 'pcLoginSelecionaCursoForm';
    this.toaster = NotifyService;
    this.state = $state;
    this.CursoAlunoService = CursoAlunoService;
    this.AuthService = AuthService;
    this.user_avatar = AuthService.getUserCredential('user_avatar');
    this.user_email = AuthService.getUserCredential('user_email');
    this.logoUrl = logo;
    this.cursos = [];
    this.loginLoading = true;
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;
    this.MensagensInfoEvolucaoConstante = MensagensInfoEvolucaoConstante;
    this.CertificacaoService = CertificacaoService;

    const name = this.AuthService.getUserCredential('user_name').split(' ');
    if (name.length > 1) {
      this.user_name = name[0] + ' ' + name[name.length - 1];
    }
    else {
      this.user_name = this.AuthService.getUserCredential('user_name');
    }

    if (!parseInt(this.AuthService.getUserCredential('user_entidade')))      {
      console.log($state.go,'state');
      $state.go('login');
    }

    this.carregarCursos();

  }

  /**
   * Retorna os cursos do Aluno
   */
  carregarCursos() {

    this.CursoAlunoService.getResource().getAll({ bl_novoportal : true}, (response) => {

      var aluno = this.AuthService.getUserCredential('user_name');

      if (response.length) {
        this.toaster.notify('success','', 'Olá, '+aluno+'. Seja bem-vindo(a) ao Portal do Aluno.');
        this.entidades = response;
        angular.forEach(response, (value,) => {
          angular.forEach(value.cursos, (valuec) => {
            this.cursos.push(valuec);
          });
        });

        this.adicionaStatusCursos();

        /* Se o aluno possuir apenas um curso e tiver evolução CURSANDO ou CONCLUINTE,
        * seleciona automaticamente e o redireciona para a tela principal. */
        if (this.cursos.length === 1 && this.cursos[0].loginPermitido) {
          this.selecionarCurso(this.cursos[0]);
        }
        this.loginLoading = false;
      }
      else {
        this.toaster.notify('warning', '', 'Nenhuma matrícula ativa encontrada no Portal do Aluno.');
        this.state.go('login');
      }
    }, () => {
      this.toaster.notify('error', '', 'Ocorreu um erro ao tentar fazer seu acesso, por favor, caso este erro persista entre em contato com sua entidade.');
      this.state.go('login');
    });
  }

  /**
   * Salva os dados do curso no storage
   * @param cursoObj
   */
  selecionarCurso(cursoObj) {
    //Ao selecionar o curso,  itera as variáveis do localStorage e apaga o conteúdo daquelas relacionadas ao bot
    for (var key in localStorage){
      if (key.indexOf('dt.session') >= 0){
        localStorage.setItem(key, '');
      }
    }
    
    //Só loga no curso caso a evolução permita login.
    if (cursoObj.loginPermitido) {
      //recupera o id da entidade setado no storage
      this.CursoAlunoService.setStorageCursoAtivo(cursoObj);
      this.state.go("app.curso", {idMatriculaAtiva: cursoObj.id_matricula});
      
      //Este reload é importante para que o bot seja recarregado.
      location.reload();
    }
  }

  /** Percorre a lista de cursos e, baseando-se na evolução seta-se mensagens de erro e a string com a classe
   * que desabilita o clique na seleção de cursos para evoluções com acesso impedido. */

  adicionaStatusCursos() {

    angular.forEach(this.cursos, (valor) => {

      // Casting necessário para o funcionamento do Switch e comparação type-safe.
      const id_evolucao = Number(valor.id_evolucao);

      /* Caso evolução for CERTIFICADO, calcula a diferença entre a data de envio do aluno e a data atual.
      Se a diferença for maior que 30 dias, desabilita o acesso ao portal do aluno e seta a mensagem
      de certificação após 30 dias. */

      if (id_evolucao === this.EvolucaoMatriculaConstante.CERTIFICADO) {
        this.CertificacaoService.detalhesCertificacaoMatricula(valor.id_matricula).then((success) => {

          // dt.envioaluno === null quando não há registro no banco de dados.
          if (success.dt_envioaluno) {
            const dataEnvioAluno = moment(success.dt_envioaluno.date);
            const dataAtual = moment();

            // Calcula a diferença em dias.
            const diferencaDias = dataAtual.diff(dataEnvioAluno, 'days');

            if (diferencaDias > 30) {
              valor.st_mensagem = this.MensagensInfoEvolucaoConstante.CERTIFICADO_APOS_30_DIAS;
              valor.loginPermitido = 0;
            }
          }
          else {
            valor.st_mensagem = null;
            valor.loginPermitido = 1;
          }
        });
      }

      /* Caso seja qualquer uma das outras evoluções, seta a mensagem de informação e a classe que desabilita
      o acesso ao portal para evoluções com acesso impedido. */

      else {
        switch (id_evolucao) {
        case this.EvolucaoMatriculaConstante.ANULADO:
        case this.EvolucaoMatriculaConstante.CANCELADO:
        case this.EvolucaoMatriculaConstante.DECURSO_DE_PRAZO:
          valor.st_mensagem = this.MensagensInfoEvolucaoConstante.PERFIL_DESATIVADO;
          valor.loginPermitido = 0;
          break;
        default:
          valor.st_mensagem = null;
          valor.loginPermitido = 1;
          break;
        }
      }
    });
  }

  /* Redireciona para a tela de login. Utilizado no botão "voltar" da tela de seleção de cursos. */
  redirecionaTelaLogin = () => {
    this.state.go('login');
  }

}

export default PcLoginSelecionaCursoFormController;
