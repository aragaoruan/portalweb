import template from './pcLoginSelecionaCursoForm.html';
import controller from './pcLoginSelecionaCursoForm.controller.js';
import './pcLoginSelecionaCursoForm.scss';


const pcLoginSelecionaCursoFormComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcLoginSelecionaCursoFormComponent;
