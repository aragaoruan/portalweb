import PcLoginFormModule from './pcLoginForm';
import PcLoginFormController from './pcLoginForm.controller';
import PcLoginFormComponent from './pcLoginForm.component';
import PcLoginFormTemplate from './pcLoginForm.html';
import config from '../../../config';


describe('PcLoginForm', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcLoginFormModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcLoginFormController(config);
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Component', () => {
      // component/directive specs
    const component = PcLoginFormComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcLoginFormTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcLoginFormController);
    });
  });
});
