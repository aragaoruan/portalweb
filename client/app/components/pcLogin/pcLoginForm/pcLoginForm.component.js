import template from './pcLoginForm.html';
import controller from './pcLoginForm.controller';
import './pcLoginForm.scss';

//Imagens estáticas


const pcLoginFormComponent = {
  restrict: 'E',
  bindings: {
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcLoginFormComponent;
