/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import componente do modulo
import pcModalConfirmComponent from './pcModalConfirm.component';


//Modulo principal do componente
const PcModalConfirm = angular.module('pcModalConfirm', ['ui.bootstrap'] )
  .component('pcModalConfirm', pcModalConfirmComponent);

export default PcModalConfirm;
