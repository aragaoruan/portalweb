import template from './pcModalConfirm.html';

// import controller from './pcModalConfirm.controller';

const pcModalConfirmComponent = {
  template,
  bindings: {
    modalInstance: "<",
    resolve: "<"
  },
  controller: [function () {

    const $ctrl = this;

    $ctrl.question  = $ctrl.resolve.data.question;
    $ctrl.title     = $ctrl.resolve.data.title;
    $ctrl.showCancelButton = ( angular.isDefined($ctrl.resolve.data.showCancelButton) ? $ctrl.resolve.data.showCancelButton : true );
    $ctrl.okText = ( angular.isDefined($ctrl.resolve.data.okText) ? $ctrl.resolve.data.okText : 'Sim' );
    $ctrl.cancelText = ( angular.isDefined($ctrl.resolve.data.cancelText) ? $ctrl.resolve.data.cancelText : 'Não' );

    $ctrl.close = function () {
      $ctrl.modalInstance.close(true);
    };

    $ctrl.cancel = function () {
      $ctrl.modalInstance.dismiss(false);
    };

  }]
};

export default pcModalConfirmComponent;
