class PcPergamumMainController {
  constructor(NotifyService, PergamumService, $state, $window, $sce) {
    'ngInject';

    this.NotifyService = NotifyService;
    this.PergamumService = PergamumService;

    this.params = $state.params;
    this.$window = $window;
    this.$sce = $sce;

    this.status = 1;
    this.url = 'http://pergamum.unyleya.edu.br:81/pergamum/biblioteca/index.php';
    this.st_caminho = this.$sce.trustAsResourceUrl(this.url);

    const win = this.$window.open(this.url, '_blank');

    if (win) {
      win.focus();
    }
    else {
      this.NotifyService.notify('warning', '', 'É necessário permitir que o navegador abra uma nova aba.');
    }
  }
}

export default PcPergamumMainController;
