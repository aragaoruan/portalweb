import template from './pcPergamumMain.html';
import controller from './pcPergamumMain.controller';
import './pcPergamumMain.scss';

const pcPergamumMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcPergamumMainComponent;
