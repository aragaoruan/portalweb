import template from './pcLayout.html';
import controller from './pcLayout.controller';
import './pcLayout.scss';

const pcLayoutComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcLayoutComponent;
