import loadingGif from '../../../assets/img/loading.gif';

class PcLayoutController {
  constructor() {
    this.name = 'pcLayout';
    this.loadingGif = loadingGif;
  }
}

export default PcLayoutController;
