import PcMeusDocumentosMainModule from './pcMeusDocumentosMain';
import PcMeusDocumentosMainController from './pcMeusDocumentosMain.controller';
import PcMeusDocumentosMainComponent from './pcMeusDocumentosMain.component';
import PcMeusDocumentosMainTemplate from './pcMeusDocumentosMain.html';

describe('PcMeusDocumentosMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcMeusDocumentosMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcMeusDocumentosMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcMeusDocumentosMainComponent;

    it('includes the intended template',() => {
        expect(component.template).to.equal(PcMeusDocumentosMainTemplate);
      });

    it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

    it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcMeusDocumentosMainController);
      });
  });
});
