import loadingGif from '../../../../assets/img/loading.gif';

class PcPromocoesListController {

  constructor(CONFIG, CampanhaComercialService, $state, NotifyService, $rootScope) {

    'ngInject';

    this.CampanhaComercialService = CampanhaComercialService;

    this.$state = $state;

    this.name = 'pcPromocoesList';

    this.loadingGif = loadingGif;

    this.loading = false;

    this.promocoes = [];

    //Mostra mensagem de erro caso ocorra algum erro na requisição de buscas as campanhas
    this.toaster = NotifyService;

    //Chama o método de buscar as campanhas
    this.buscarCampanhaComercial(true);

    this.activeId = $state.params.id_campanhacomercial ? $state.params.id_campanhacomercial : null;

    this.$rootScope = $rootScope;

    this.$rootScope.$on('navegarPromocoesEvent', ( event, data ) => {
      this.activeId = data.id_campanhacomercial;
    });

  }

  count = () => {
    this.CampanhaComercialService.getResource().count({ id_matricula: this.$state.params.idMatriculaAtiva, bl_portaldoaluno: 1 }, (response) => {
      this.$rootScope.countPromocoes = response.total;
    });

  };

 /**
    Abre os detalhes de de uma campanha (show)
 **/
  visualizarCampanhaComercial = (event, campanha, search = true) => {
    if (event){
      event.preventDefault();
    }
    this.activeId = campanha.id_campanhacomercial;
    this.count();
    if (search) {
      this.$state.go('app.promocoes.show', {id_campanhacomercial: campanha.id_campanhacomercial, id_matricula: this.$state.params.idMatriculaAtiva }, {reload: false});
    }
  }

  /**
  Busca as campanhas do usuário logado
  Listagem
  **/
  buscarCampanhaComercial = (buscar = false) => {

    this.loading = true;

    this.CampanhaComercialService.getResource().getAll({ bl_portaldoaluno: 1, id_matricula: this.$state.params.idMatriculaAtiva, order: 'dt_inicio', direction: 'desc' , 'columns[]' : [ 'id_campanhacomercial', 'st_campanhacomercial','st_descricao','st_imagem','st_link','dt_inicio','dt_fim', 'bl_visualizado'] }, (response) => {
      this.loading = false;
      this.promocoes = response;

      if (response.length > 0) {
        this.visualizarCampanhaComercial(null, response[0], buscar);
      }

    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });

  }
}

export default PcPromocoesListController;
