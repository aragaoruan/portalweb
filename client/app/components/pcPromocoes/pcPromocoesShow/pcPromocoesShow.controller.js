import loadingGif from "../../../../assets/img/loading.gif";
import _ from 'lodash';

class PcPromocoesShowController {
  constructor(CONFIG, CampanhaComercialService, $state, NotifyService, $rootScope, $scope, $localStorage) {
    'ngInject';

    this.storage = $localStorage;
    this.CampanhaComercialService = CampanhaComercialService;
    this.CONFIG = CONFIG;
    this.name = 'pcPromocoesShow';
    this.$state = $state;
    this.loadingGif = loadingGif;
    this.loading = false;
    this.toaster = NotifyService;
    this.scope = $scope;
    this.indexActive = -1;
    this.$rootScope = $rootScope;

    this.visualizarCampanhaComercial($state.params.id_campanhacomercial ? $state.params.id_campanhacomercial : null);

    this.buscarCampanhaComercial();

  }

  visualizarCampanhaComercial = (id_campanhacomercial) => {
    this.loading = true;
    this.CampanhaComercialService.getResource().getOneById( { id: id_campanhacomercial }, (response) => {
      this.campanha = response;
      this.loading = false;
      this.setIndexActive();
      this.count();
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });

  };

  count = () => {
    this.CampanhaComercialService.getResource().count({ id_matricula: this.$state.params.idMatriculaAtiva, bl_portaldoaluno: 1 }, (response) => {
      this.$rootScope.countPromocoes = response.total;
    });
  };


  buscarCampanhaComercial = () => {
    this.loading = true;

    if (!this.promocoes) {
      this.CampanhaComercialService.getResource().getAll({bl_portaldoaluno: 1, id_matricula: this.$state.params.idMatriculaAtiva, order: 'dt_inicio', direction: 'desc' , 'columns[]': ['id_campanhacomercial', 'st_campanhacomercial', 'st_descricao', 'st_imagem', 'st_link', 'dt_inicio', 'dt_fim']}, (response) => {
        this.loading = false;
        this.promocoes = response;
        this.setIndexActive();
        this.count();
      }, (error) => {
        this.loading = false;
        this.toaster.notify(error.data);
      });
    }

  }

  /**
   * Seta qual índice do array corresponde à mensagem exibida
   */
  setIndexActive() {
    if (this.promocoes && this.promocoes.length > 0 && this.campanha) {
      this.indexActive = _.findIndex(this.promocoes, (o) => {
        return (o.id_campanhacomercial === this.campanha.id_campanhacomercial);
      });
    }
  }

  /**
   * Método para permitir a navegação entre mensagens usando os links "anterior" e "próximo"
   * @param index
   */
  navegarPromocao(index) {
    if (this.promocoes && this.promocoes[index] && index > -1) {
      this.indexActive = index;
      this.campanha = this.promocoes[index];
      this.$rootScope.$broadcast('navegarPromocoesEvent', this.campanha);
      this.$state.go('app.promocoes.show', {id_campanhacomercial: this.campanha.id_campanhacomercial}, {notify: false});
    }
    else {
      this.toaster.notify('error', '', 'Promoção não encontrada!');
    }
  }


}

export default PcPromocoesShowController;
