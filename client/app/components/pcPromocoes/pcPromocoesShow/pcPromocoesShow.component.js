import template from './pcPromocoesShow.html';
import controller from './pcPromocoesShow.controller';
import './pcPromocoesShow.scss';

const pcPromocoesShowComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcPromocoesShowComponent;
