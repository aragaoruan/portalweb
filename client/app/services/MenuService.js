import RestService from './RestService';

/**
 * Classe MenuService
 * @author Gabriel Resende <gabriel.resende@unyleya.com.br>
 */
class MenuService extends RestService {

    /**
     * Metodo construtor
     * @param CONFIG
     * @param $resource
     * @param $http
     * @author Gabriel Resende <gabriel.resende@unyleya.com.br>
     */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

        //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'menu';
    this.CONFIG = CONFIG;

    this.addCustomService('getMenuSideBar', CONFIG.urlApi + '/menu/menu-side-bar', 'GET', {}, true);

  }

    /**
     * retorna os dados do menu
     * @param idEntidade
     * @returns {Function}
     */
  getMenuSideBar(idEntidade) {

    const defer = this.q.defer();

    this.getResource()
            .getMenuSideBar({ id_entidade: idEntidade }, (response) => {
              defer.resolve(response);
            }, (error) => {
              defer.reject(error);
            });
    return defer.promise;
  }

}

export default MenuService;
