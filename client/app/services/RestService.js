/**
 * Service principal com metodos comumns e basicos a serem utilizados na camada de services.
 * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
 */
class RestService {

  //Metodo construtor
  constructor(CONFIG, $resource, $http, $q, $localStorage) {
    'ngInject';
    this.storage = $localStorage;
    this.api = "";
    this.$resource = $resource;
    this.CONFIG = CONFIG;
    this.http = $http;
    this.q = $q;

  }

  /**
   * Metodo que retorna a factorys de recursos angular com metodos comuns e customizados emcapsulados para uso.
   * @returns {Object|*}
   * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
   */
  getResource = (newCustomResource = {}) => {
    //Verificando se a service que estende a service principal, tem definido o atributo "API"
    if (!this.api) {
      throw 'Api nao informado em: ' + this.constructor.name;
    }

    //Retorna service angular resource com servicos comuns e costomizados emcapsulados
    return this.$resource(
      this.CONFIG.urlApi + '/' + this.api, {},
      angular.extend({}, this.commomServices(this.CONFIG.urlApi + '/' + this.api), this.customServices, newCustomResource)
    );
  }

  /**
   * Funcao utilizada para adicionar servicos customizados ao resource
   * @param name
   * @param url
   * @param method
   * @param params
   * @param isArray
   * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
   */
  addCustomService(name, url, method, params, isArray = false) {
    const custom = [];

    //Criando objeto de novo recurso a ser registrado
    custom[name] = {
      url,
      method,
      params,
      isArray
    };

    //Juntando recursos customizados e comuns
    this.customServices = angular.extend({}, this.customServices, custom);
  }

  /**
   * Reescrita do método getOneById, este método pode ser usado na Service de duas formas
   * ServiceName.getOneById ou
   * ServiceName.getResource().getOneById({id:id}, func.... ( neste momento, você não está chamando este método em si, mas a declaração no Resource )
   * @param id
   * @returns {*}
   */
  getOneById(id) {
    return this.q((resolve, reject) => {
      this.getResource().getOneById({
        id: id
      }, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Reescrita do método getAll, este método pode ser usado na Service de duas formas
   * ServiceName.getAll ou
   * ServiceName.getResource().getAll({...}, func.... ( neste momento, você não está chamando este método em si, mas a declaração no Resource )
   * @param params
   * @returns {*}
   */
  getAll(params = {}) {
    return this.q((resolve, reject) => {
      this.getResource().getAll(params, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Definicao de servicos comuns para todas as services
   * @param api
   * @returns {{getAll: {url: *, method: string}, getOneBy: {url: string, method: string}, create: {url: string, method: string}, update: {url: string, method: string}, delete: {url: string, method: string}}}
   * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
   */
  commomServices(api) {
    return {
      getAll: {
        url: api,
        isArray: true
      },
      getOneById: {
        url: api + '/get/id/:id',
        method: 'GET'
      },
      getOneBy: {
        url: api + '/get',
        method: 'GET'
      },
      create: {
        url: api + '/create',
        method: 'POST'
      },
      update: {
        url: api + '/update/id/:id',
        method: 'PUT'
      },
      delete: {
        url: api + '/delete/id/:id',
        method: 'DELETE'
      }
    };
  }
}

export default RestService;
