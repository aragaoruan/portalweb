/**
 * Neemias Santos <neemias.santos@unyleya.com.br>
 */
import RestService from './RestService';

/**
 * Classe PessoaService
 * @author
 */
class PessoaService extends RestService {

    /**
     *
     * @author Neemias Santos <neemias.santos@unyleya.com.br>
     * @param CONFIG
     * @param $resource
     * @param $http
     * @param $q
     * @param LSService
     */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService);

        //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'pessoa';

    this.addCustomService('getDadosPessoa', CONFIG.urlApi + '/pessoa/dados-pessoa', 'GET', {});
    this.addCustomService('getVwPessoaEndereco', CONFIG.urlApi + '/endereco/vw-pessoa-endereco', 'GET', {});
  }


    /**
     * Retorna os dados da vw_pessoa
     * @param idUsuario
     * @returns {Function}
     */
  getDadosPessoa(idUsuario, idEntidade) {

    const defer = this.q.defer();

    this.getResource()
            .getDadosPessoa({id_usuario: idUsuario, id_entidade: idEntidade}, (response) => {
              defer.resolve(response);
            }, (error) => {
              defer.reject(error);
            });
    return defer.promise;
  }

    /**
     * Retorna os dados pessoa endereco
     * @param idUsuario
     * @param idTipoEndereco
     * @returns {Function}
     */
  getVwPessoaEndereco(idUsuario, idTipoEndereco) {

    const defer = this.q.defer();

    this.getResource().getVwPessoaEndereco({
      id_usuario: idUsuario,
      id_tipoendereco: idTipoEndereco
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

}

export default PessoaService;
