import alertify from "alertifyjs/build/alertify.min.js";

/**
* Service principal para notificações, pode ser evoluída para termos confirms e outros tipos
* @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
*/
class NotifyService {

  //Metodo construtor
  constructor($log) {
    'ngInject';
    this.log = $log;
    this.alertify = alertify;

    alertify.set('notifier','position', 'top-right');
  }

  /**
  * Envia uma notificação
  * @param type
  * @param title
  * @param message
  * @returns {*|T}
  */
  notify(type = 'success', title = 'OK', message = '') {

    try {
      if (angular.isString(type)) {
        switch (type) {
        case 'success':
          return  alertify.success(title + " " +message);
        case 'error':
          return  alertify.error(title + " " +message);
        case 'warning':
          return  alertify.warning(title + " " +message);
        default:
          return  alertify.success(title + " " +message);
        }
      }
      if (angular.isObject(type)) {
        type.type || (type.type = 'success');
        type.title || (type.title = 'Sucesso');
        type.message || (type.message = '');
        switch (type.type) {
        case 'success':
          return  alertify.success(type.title + " " +type.message);
        case 'error':
          return  alertify.error(type.title + " " +type.message);
        case 'warning':
          return  alertify.warning(type.title + " " +type.message);
        default:
          return  alertify.success(type.title + " " +type.message);
        }
      }
    }
    catch (err) {
      this.log.error(err);
    }
  }

}

export default NotifyService;
