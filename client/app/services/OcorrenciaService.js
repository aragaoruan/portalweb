import RestService from './RestService';

/**
 * Classe OcorrenciaService
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class OcorrenciaService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
  */
  constructor(CONFIG, $resource, $http, $q, LSService, Upload) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    this.api = 'ocorrencia';
    this.Upload = Upload;
    this.addCustomService('reabrirChamado', CONFIG.urlApi + '/ocorrencia/reabrir', 'POST', {});
    this.addCustomService('encerrarChamado', CONFIG.urlApi + '/ocorrencia/encerrar' , 'POST', {});
    this.addCustomService('assuntos', CONFIG.urlApi + '/ocorrencia/assuntos' , 'GET', {}, true);
  }

  /**
   * Reabre um chamado
   * @param id_ocorrencia (integer )
   * @returns {Function|promise}
   * @author Denise Xavier <denise.xavier@unyleya.com.br>
   */
  reabrirChamado(id_ocorrencia) {

    const defer = this.q.defer();

    this.getResource().reabrirChamado({id_ocorrencia}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  /**
   * Encerra um chamado
   * @param id_ocorrencia (integer )
   * @returns {Function|promise}
   * @author Denise Xavier <denise.xavier@unyleya.com.br>
   */
  encerrarChamado(id_ocorrencia) {
    const defer = this.q.defer();

    this.getResource().encerrarChamado({id_ocorrencia}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }


 /**
  * Retorna os assuntos
  * @param array param
  * @returns {Function|promise}
  * @author Denise Xavier <denise.xavier@unyleya.com.br>
  */
  assuntos (param) {
    const defer = this.q.defer();

    this.getResource().assuntos( param , (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  /**
   * Cria uma nova ocorrencia, enviando o arquivo anexado
   * @param ocorrencia
   * @returns {*}
   * @author Denise Xavier <denise.xavier@unyleya.com.br>
   * @author UPDATE Neemias Santos <neemias.santos@unyleya.com.br>
   */
  create (ocorrencia) {
    return this.Upload.upload({
      url: this.CONFIG.urlApi +  '/ocorrencia/create',
      data: ocorrencia
    });
  }

}

export default OcorrenciaService;
