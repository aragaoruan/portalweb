import RestService from "./RestService";

class AvisosService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService,$state,$uibModal) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'avisos';
    this.state = $state;
    this.avisoObj = {
      rota: '',
      params: {}
    };
    this.modal = $uibModal;


  }

  /**
   * Funcao para retornar os icones de acordo com cada tipo de mensagem
   * @param type
   * @returns {*}
   */
  getIconAviso = (type) => {
    let icone;
    switch (type) {
    case 'mensagem':
    default:
      icone = 'icon-icon_chat';
      break;
    }

    return icone;

  }

  getFunctionAviso = (aviso, matriculaAtiva) => {
    switch (aviso.type) {
    case 'mensagem':
      this.avisoObj.rota = "app.mensagens.show";
      this.avisoObj.params = {
        idMatriculaMensagem: aviso.object.id_matricula,
        id_enviomensagem: aviso.object.id_enviomensagem
      };
      this.functionAviso =  () => {
        this.state.go(this.avisoObj.rota, this.avisoObj.params);
      };
      break;
    case 'envioTcc':
      this.functionAviso =  () => {

        return this.modal.open({
          component: "PwEnvioTccForm",
          resolve: {
            dadostcc: () => {
              return aviso.object;
            }
          }
        });

      };
      break;
    case 'agendamento':
      this.functionAviso = () => {
        this.state.go('app.agendamento', {idMatriculaAtiva: matriculaAtiva});
      };
      break;
    }
    this.avisoObj.params.idMatriculaAtiva = matriculaAtiva;
    return this.functionAviso;
  }

}

export default AvisosService;
