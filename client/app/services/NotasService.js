import RestService from './RestService';

/**
 * Class NotasService
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class NotasService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    "ngInject";
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'notas';
    this.CONFIG = CONFIG;
    this.addCustomService('getGradeNotas', CONFIG.urlApi + '/grade-nota/get-vw-grade-nota-agrupada', 'GET', {});
    this.addCustomService('getGradeNotasDetalhada', CONFIG.urlApi + '/grade-nota/get-vw-grade-nota-agrupada-detalhada', 'GET', {});
  }

  getGradeNotas(idMatricula) {
    var defer = this.q.defer();
    this.getResource()
      .getGradeNotas({id_matricula : idMatricula},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

  getGradeNotasDetalhada(idMatricula, idDisciplina) {
    var defer = this.q.defer();
    this.getResource()
      .getGradeNotasDetalhada({id_matricula : idMatricula, id_disciplina: idDisciplina},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

}

export default NotasService;
