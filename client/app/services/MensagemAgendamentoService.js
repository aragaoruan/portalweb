import RestService from './RestService';

/**
 * Classe MensagemAgendamentoService
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 */
class MensagemAgendamentoService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @author Elcio Guimarães <elcioguimaraes@gmail.com>
   */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'mensagem-agendamento';

    this.addCustomService('updateEvolucao', CONFIG.urlApi + '/mensagem-agendamento/update-evolucao', 'PUT', {});

  }

  /**
   * Atualiza a evolução de uma mensagem
   * @param data
     */
  updateEvolucao(data) {

    const defer = this.q.defer();

    this.getResource()
      .updateEvolucao(data, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }
}

export default MensagemAgendamentoService;
