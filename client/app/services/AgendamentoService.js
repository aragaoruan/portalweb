import RestService from './RestService';

/**
 *
 * Classe para tratar o Agendamento de Avaliação
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 *
 */
class AgendamentoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'agendamento';

    this.addCustomService('getSituacaoAgendamento', CONFIG.urlApi + '/agendamento/verificar-situacao-agendamento', 'GET', {}, false);
    this.addCustomService('getDisciplinasAgendamento', CONFIG.urlApi + '/agendamento/disciplinas', 'GET', {}, true);
    this.addCustomService('getDatasDisponiveis', CONFIG.urlApi + '/agendamento/datas-disponiveis', 'GET', {}, true);
    this.addCustomService('getAgendamentoComprovante', CONFIG.urlApi + '/agendamento/get-agendamento-comprovante', 'GET', {}, false);
  }

  getSituacaoAgendamento(idMatricula) {
    const defer = this.q.defer();

    this.getResource().getSituacaoAgendamento({
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retorna as disciplinas do agendamento do aluno
   * @param idMatricula
   * @returns {*}
   */
  getDisciplinasAgendamento(idMatricula) {
    const defer = this.q.defer();

    this.getResource().getDisciplinasAgendamento({
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retorna as datas disponíveis para o agendamento
   * @param sgUf
   * @param idMunicipio
   * @returns {*}
   */
  getDatasDisponiveis(sgUf, idMunicipio) {
    const defer = this.q.defer();

    this.getResource().getDatasDisponiveis({
      sg_uf: sgUf,
      id_municipio: idMunicipio
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  getAgendamentoComprovante(idMatricula) {
    const defer = this.q.defer();

    this.getResource().getAgendamentoComprovante({
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default AgendamentoService;
