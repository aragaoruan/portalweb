import RestService from './RestService';

/**
 * Classe InteracaoService
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class InteracaoService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @param $q
   * @param LSService
   * @param Upload
   */
  constructor(CONFIG, $resource, $http, $q, LSService, Upload) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    this.api = 'interacao';
    this.Upload = Upload;
    this.addCustomService('novaInteracao', CONFIG.urlApi + '/interacao/create', 'POST', {});
  }

  /**
   * Cria uma nova interação para o chamado, enviando o anexo e outros dados necessarios
   * @param tramite (st_tramite, id_entidade, id_evolucao, id_usuariointeressado, bl_visivel, file, id_ocorrencia)
   * @returns {Function|promise}
   * @author Denise Xavier <denise.xavier@unyleya.com.br>
   */
  novaInteracao(tramite) {
    return this.Upload.upload({
      url: this.CONFIG.urlApi + '/interacao/create',
      data: tramite
    });
  }
}

export default InteracaoService;
