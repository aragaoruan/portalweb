import RestService from './RestService';

/**
 * Classe FinanceiroService
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 */
class FinanceiroService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @author Elcio Guimarães <elcioguimaraes@gmail.com>
   */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'resumo-financeiro';

    this.CONFIG = CONFIG;

    this.addCustomService('getVendasUser', CONFIG.urlApi + '/resumo-financeiro', 'GET', {}, true);
    this.addCustomService('getDadosBoleto', CONFIG.urlApi + '/boleto/gerar-boleto', 'GET', {});
    this.addCustomService('getUrlBoleto', CONFIG.urlApi + '/boleto/url-boleto', 'GET', {});
    this.addCustomService('getStatusPlanoPagamento', CONFIG.urlApi + '/resumo-financeiro/existe-plano-pagamento', 'GET', {});
    this.addCustomService('getManualDoAluno', CONFIG.urlApi + '/manual-do-aluno', 'GET', {});
  }


  /**
   * Retorna o resumo financeiro do usuário
   */
  getVendasUser() {

    const defer = this.q.defer();

    this.getResource()
      .getVendasUser({}, (response) => { //funcao login
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }

  /**
   * Retorna a URL do Boleto
   * @param id_lancamento
   */
  getUrlBoleto(id_lancamento) {

    const defer = this.q.defer();

    this.getResource()
      .getUrlBoleto({id_lancamento:id_lancamento}, (response) => { //funcao login
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }


  /**
   * Pega os dados do Boleto
   * @param id_lancamento
   * @param funcao
   * @returns {Function|promise}
   * @author Elcio Guimarães <elcioguimaraes@gmail.com>
   */
  getDadosBoleto(id_lancamento, funcao = 'getbarcode') {


    const defer = this.q.defer();

    this.getResource()
      .getDadosBoleto({funcao, id_lancamento}, (response) => { //funcao login
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }

  getStatusPlanoPagamento(idMatricula, idEntidade){
    const defer = this.q.defer();
    this.getResource()
      .getStatusPlanoPagamento({id_matricula: idMatricula, id_entidade: idEntidade},
        (response) => {
          defer.resolve(response);
        }, (response) => {
          defer.reject(response);
        });
    return defer.promise;
  }

  getManualDoAluno(idEntidade) {
    const defer = this.q.defer();
    this.getResource()
      .getManualDoAluno({id_entidade: idEntidade},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

}

export default FinanceiroService;
