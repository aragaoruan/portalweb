//Importe do modulo angular resources
import angularResource from "angular-resource";
import Configs from "../config.js";
import Constants from "../constants/constants.js";
import LSService from "./LSService";
import AuthService from "./AuthService";
import NotifyService from "./NotifyService";
import CursoAlunoService from "./CursoAlunoService";
import MensagemAgendamentoService from "./MensagemAgendamentoService";
import DisciplinaAlunoService from "./DisciplinaAlunoService";
import FinanceiroService from "./FinanceiroService";
import OcorrenciaService from "./OcorrenciaService";
import InteracaoService from "./InteracaoService";
import PessoaService from "./PessoaService";
import DownloadService from "./DownloadService";
import AvisosService from "./AvisosService";
import MenuService from './MenuService';
import CursoService from './CursoService';
import NotasService from './NotasService';
import MoodleService from './MoodleService';
import TabelaPrecoService from './TabelaPrecoService';
import TccService from './TccService';
import PrimeiroAcessoService from './PrimeiroAcessoService';
import AtividadeComplementarService from './AtividadeComplementarService';
import TipoAtividadeComplementarService from './TipoAtividadeComplementarService';
import UnyleyaMoodleService from './UnyleyaMoodleService';
import AgendamentoService from './AgendamentoService';
import EnderecoService from './EnderecoService';
import PearsonService from './PearsonService';
import PergamumService from './PergamumService';
import CertificacaoService from './CertificacaoService';
import CampanhaComercialService from './CampanhaComercialService';
import VistaDeProvaService from './VistaDeProvaService';
import LinhaDeNegocioService from './LinhaDeNegocioService';
import "angular-md5/angular-md5.min";

//Importe de modulo de configuracoes da aplicacao

/**
 * Modulo principal para camada de servicos com definicoes gerais
 */
export default angular.module('Services', [angularResource, Configs.name, Constants.name, 'angular-md5'])
  .config(($httpProvider, $resourceProvider) => {

    $httpProvider.interceptors.push('httpInterceptor');

    // Don't strip trailing slashes from calculated URLs
    $resourceProvider.defaults.striptrailingSlashes = false;
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    $httpProvider.defaults.useXDomain = true;

    // $httpProvider.defaults.headers.common["Cache-Control"] = 'no-transform';
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

  })

  /**
   * Interceptor para adicionar os tokens de autenticação somente quando chamarmos a API G2
   * Aqui devems er adicionadas as URLs que precisam destes tokens
   */
  .factory('httpInterceptor', ['CONFIG', '$localStorage', (CONFIG, $localStorage) => {
    var httpInterceptor = {
      request: function (config) {
        if (config.url.indexOf(CONFIG.urlApi) > -1) {
          config.headers = {
            public_token: CONFIG.tokenApi,
            user_token: $localStorage.user_token
          };
        }

        return config;
      }
    };
    return httpInterceptor;
  }])

  /**
   * Importar cada service da aplicacao no seguinte formato:
   *
   * Exemplo:
   * import AuthService from './AuthService';
   * Services.service('AuthService', AuthService);
   */


  .service('LSService', LSService)

  .service('AuthService', AuthService)

  .service('NotifyService', NotifyService)

  .service('CursoAlunoService', CursoAlunoService)

  .service('MensagemAgendamentoService', MensagemAgendamentoService)

  .service('DisciplinaAlunoService', DisciplinaAlunoService)

  .service('FinanceiroService', FinanceiroService)

  .service('OcorrenciaService', OcorrenciaService)

  .service('InteracaoService', InteracaoService)

  .service('PessoaService', PessoaService)

  .service('MoodleService', MoodleService)

  .service('DownloadService', DownloadService)

  .service('AvisosService', AvisosService)

  .service('MenuService', MenuService)

  .service('NotasService', NotasService)

  .service('CursoService', CursoService)

  .service('TabelaPrecoService', TabelaPrecoService)

  .service('TccService', TccService)

  .service('PrimeiroAcessoService', PrimeiroAcessoService)

  .service('UnyleyaMoodleService', UnyleyaMoodleService)

  .service('PearsonService', PearsonService)

  .service('PergamumService', PergamumService)

  .service('AgendamentoService', AgendamentoService)

  .service('AtividadeComplementarService', AtividadeComplementarService)

  .service('TipoAtividadeComplementarService', TipoAtividadeComplementarService)

  .service('EnderecoService', EnderecoService)

  .service('CampanhaComercialService', CampanhaComercialService)

  .service('CertificacaoService', CertificacaoService)

  .service('VistaDeProvaService', VistaDeProvaService)

  .service('LinhaDeNegocioService', LinhaDeNegocioService);
