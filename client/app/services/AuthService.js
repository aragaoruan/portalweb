//Import classe RestService
import RestService from "./RestService";
import jwt from "jwt-simple";

//Import biblioteca JWT, utilizada para codificar e decodificar token de usuario

/**
* Classe AuthService para servicos de autenticacao
* @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
*/
class AuthService extends RestService {

  /**
  * Metodo construtor
  * @param CONFIG
  * @param $resource
  * @param $http
  * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
  */
  constructor(CONFIG, $resource, $http, $q, $localStorage ,CursoAlunoService, TbSistema) {

    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, $localStorage); //Entrega para a classe pai as factorys e services injetadas na classe controller
    this.storage = $localStorage;
    this.addCustomService('login', CONFIG.urlApi + '/auth/login', 'POST', {});
    this.CursoAlunoService = CursoAlunoService;
    this.TbSistema = TbSistema;

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'auth';

    //Utilizando funcao para adicionar aos servicos, o servico customizado login
    this.addCustomService('login', CONFIG.urlApi + '/auth/login', 'POST', {});
    this.addCustomService('autenticacaoDireta', CONFIG.urlApi + '/auth/autenticacao-direta', 'POST', {});
    this.addCustomService('recuperarSenha', CONFIG.urlApi + '/auth/recuperar-senha', 'POST', {});
    this.addCustomService('alteraSenha', CONFIG.urlApi + '/usuario/altera-senha', 'POST', {});
    this.addCustomService('gravaLogAcesso', CONFIG.urlApi + '/log/create', 'POST', {});
  }

  /**
  * Metodo que verifica se o usuario contem um token de autenticacao e se o mesmo tem validade
  * @returns {boolean}
  * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
  */
  isAuthenticated() {
    /**
    * Funcao do try e tentar verificar se a tentativa de decodificar o token tem resultado positivo,
    * verificando-se tambem a veracidade do token.
    */
    try {
      //Decodifica o token
      const tokenDecoded = this.jwtDecode(this.storage.user_token);

      //Verifica se o token decodificado retorna uma informacao similar a informacao gravada no storage
      if (tokenDecoded.login ===  this.storage.login) {
        return true;
      }
    }
    catch (error) {
      return false;
    }
  }

  /**
  * Metodo para fazer decode do token do usuario
  * @param token
  * @returns {*}
  * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
  */
  jwtDecode(token) {
    return jwt.decode(token, this.CONFIG.tokenApi, this.CONFIG.tokenTypeAlg);
  }

  /**
  * Funcao para fazer login de um usuario
  * @param user
  * @returns {Function|promise}
  * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
  */
  login(user) {

    const that = this;

    //Cria um promise para ser utilizado na funcao, com intencao
    const defer = this.q.defer();

    //Utilizando variavel local AuthService que devolve instancia da service AuthService.
    this.getResource() //Pega resource da service
    .login(user, (response) => { //funcao login
      //Utilizando metodo da service AuthService para gravar dados de usuario autenticado
      that.storeUserCredentials(response.token);

      //Grava uma solucao no promise
      defer.resolve(response);
    }, (error) => {
      //Grava uma solucao negativa no primise
      defer.reject(error);
    });

    //Retorna o promise
    return defer.promise;
  }

  /**
   * Faz a autenticação direta ao portal do aluno
   * @param blGravaLog
   * @param idUsuarioOriginal
   * @param idEntidade
   * @param idUsuario
   * @param stUrlAcesso
     * @returns {Function}
     */
  autenticacaoDireta(blGravaLog, idUsuarioOriginal, idEntidade, idUsuario, stUrlAcesso) {

    var that = this;

    //Cria um promise para ser utilizado na funcao, com intencao
    var defer = this.q.defer();

    //Utilizando variavel local AuthService que devolve instancia da service AuthService.
    this.getResource() //Pega resource da service
      .autenticacaoDireta({
        bl_gravalog: blGravaLog
        ,id_usuariooriginal: idUsuarioOriginal
        ,id_entidade: idEntidade
        ,id_usuario: idUsuario
        ,st_urlacesso: stUrlAcesso
      }, (response) => { //funcao login

        //Utilizando metodo da service AuthService para gravar dados de usuario autenticado
        that.storeUserCredentials(response.token);
        defer.resolve(response);
      }, (error) => {

        //Grava uma solucao negativa no primise
        defer.reject(error);
      });

    //Retorna o promise
    return defer.promise;
  }


  /**
  * Funcao para recuperar a senha do usuário
  * @param user
  * @returns {Function|promise}
  * @author Elcio Guimarães <elcioguimaraes@gmail.com>
  */
  recuperarSenha(user) {

    //Cria um promise para ser utilizado na funcao, com intencao
    const defer = this.q.defer();

    //Utilizando variavel local AuthService que devolve instancia da service AuthService.
    this.getResource() //Pega resource da service
    .recuperarSenha(user, (response) => { //funcao login
      //Grava uma solucao no promise
      defer.resolve(response);
    }, (error) => {
      //Grava uma solucao negativa no primise
      defer.reject(error);
    });

    //Retorna o promise
    return defer.promise;
  }

  /**
  * Grava credenciais do usuario autenticado
  * @param token
  * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
  */
  storeUserCredentials(token) {

    const tokenDecoded = this.jwtDecode(token);

    this.storage.user_token = token;
    this.storage.login = tokenDecoded.login;
    this.storage.user_name = tokenDecoded.user_name;
    this.storage.user_shortname = tokenDecoded.user_name.substr(0, tokenDecoded.user_name.indexOf(" "));
    this.storage.user_avatar = tokenDecoded.user_avatar;
    this.storage.user_entidade = tokenDecoded.entidade_id;
    this.storage.user_email = tokenDecoded.user_email;
    this.storage.user_cpf = tokenDecoded.user_cpf;
    this.storage.user_id = tokenDecoded.user_id;
    this.storage.id_usuariooriginal = tokenDecoded.id_usuariooriginal;
    this.storage.st_usuariooriginal = tokenDecoded.st_usuariooriginal;
    this.storage.bl_acessarcomo = tokenDecoded.bl_acessarcomo;

  }

  /**
  * Destroi credenciais do usuario autenticado
  * @auhor Rafael Bruno <rafael.oliveira@unyleya.com.br>
  */
  destroyUserCredentials() {
    for (var key in this.storage){

      // a segunda condição do if é necessária para destruir o bot ao refazer login/trocar de curso.
      if (key.indexOf('moodle_token') > -1 || key.indexOf('dt.session') >= 0) {
        delete this.storage[key];
      }
    }

    delete this.storage.user_token;
    delete this.storage.login;
    delete this.storage.user_name;
    delete this.storage.user_avatar;
    delete this.storage.user_entidade;
    delete this.storage.user_email;
    delete this.storage.user_id;
    delete this.storage.id_usuariooriginal;
    delete this.storage.st_usuariooriginal;
    delete this.storage.bl_acessarcomo;

  }

  getUserCredential(key) {
    return   this.storage[key];
  }

  setUserCredential(key, value) {
    this.storage[key] = value;
  }

  /**
  * Funcao para alterar a senha do usuário
  *
  * @author Neemias Santos <neemias.santos@unyleya.com.br>
  * @param idUsuario
  * @param stSenha
  * @param idEntidade
  * @returns {Function}
  */
  alteraSenha(idUsuario, stSenha, idEntidade) {
    //Cria um promise para ser utilizado na funcao, com intencao
    const defer = this.q.defer();

    //Utilizando variavel local AuthService que devolve instancia da service AuthService.
    this.getResource().alteraSenha({
      id_usuario: idUsuario,
      st_senha: stSenha,
      id_entidade: idEntidade
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  gravaLogAcesso(rota, params = null) {
    //Cria um promise para ser utilizado na funcao, com intencao
    const  defer = this.q.defer();

    const idUsuario = this.storage.user_id;
    const idEntidade =  this.storage.user_entidade;

    // let idEntidade = this.cursoAtivo.id_entidade;
    //Utilizando variavel local AuthService que devolve instancia da service AuthService.
    if (typeof rota !== angular.isUndefined && !this.storage.bl_acessarcomo) {
      this.getResource().gravaLogAcesso({
        id_usuario: idUsuario,
        id_entidade: idEntidade,
        id_sistema: this.TbSistema.PortalAluno,
        st_parametros: params,
        rota
      }, () => {

      }, () => {

      });
      return defer.promise;
    }
  }

}

export default AuthService;
