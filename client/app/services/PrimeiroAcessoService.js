/**
 * Neemias Santos <neemias.santos@unyleya.com.br>
 */
import RestService from './RestService';

/**
 * Classe PessoaService
 * @author
 */
class PrimeiroAcessoService extends RestService {

  /**
   *
   * @author Neemias Santos <neemias.santos@unyleya.com.br>
   * @param CONFIG
   * @param $resource
   * @param $http
   * @param $q
   * @param LSService
   */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService);

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'pessoa';

    this.addCustomService('getPrimeiroAcesso', CONFIG.urlApi + '/primeiro-acesso/', 'GET', {});
    this.addCustomService('postAceiteContrato', CONFIG.urlApi + '/primeiro-acesso/create', 'POST', {});
  }


  /**
   * Retorna os dados da api index
   * @param idMatricula
   * @returns {Function}
   */
  getPrimeiroAcesso(idMatricula) {
    const defer = this.q.defer();
    this.getResource().getPrimeiroAcesso({id_matricula: idMatricula}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  /**
   * Funcao para alterar salvar o aceite do contrato no primeiro acesso
   *
   * @author Neemias Santos <neemias.santos@unyleya.com.br>
   * @param idMatricula
   * @returns {Function}
   */
  postAceiteContrato(idMatricula) {
    //Cria um promise para ser utilizado na funcao, com intencao
    const defer = this.q.defer();
    this.getResource().postAceiteContrato({
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }
}

export default PrimeiroAcessoService;
