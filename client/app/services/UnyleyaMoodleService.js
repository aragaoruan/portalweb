import RestService from './RestService';

/**
 * Classe para tratar os dados do WS Unyleya Moodle
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class UnyleyaMoodleService extends RestService{

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);

    this.api = 'moodle';

    this.addCustomService('listRecentActivity', CONFIG.urlApi + '/moodle/list-recent-activity', 'GET', {}, true);

  }

  /**
   * Retorna os dados de atividades recentes do moodle
   * @param id_matricula
   * @param courseid
   * @param id_saladeaula
   * @returns {Function}
   */
  listRecentActivity = (id_matricula, courseid, id_saladeaula) => {

    const defer = this.q.defer();

    var params = {
      idMatricula: id_matricula,
      idSaladeaula: id_saladeaula,
      stCodsistemasala: courseid
    };

    this.getResource().listRecentActivity(params
      ,
      (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;

  }

}

export default UnyleyaMoodleService;
