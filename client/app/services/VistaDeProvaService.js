import RestService from './RestService';

class VistaDeProvaService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'vista-de-prova';

    this.CONFIG = CONFIG;

    this.addCustomService('getVistaDeProva', CONFIG.urlApi + '/vista-de-prova', 'GET', {});
    this.addCustomService('getVerificarPermissao', CONFIG.urlApi + '/vista-de-prova/verificar-permissao', 'GET', {});

  }

  getVerificarPermissao(idEntidade) {
    const defer = this.q.defer();

    this.getResource()
      .getVerificarPermissao({id_entidade: idEntidade}, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;

  }

  getVistaDeProva(idUsuario) {

    const defer = this.q.defer();

    this.getResource()
      .getVistaDeProva({idUsuario: idUsuario}, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }
}

export default VistaDeProvaService;
