import RestService from "./RestService";

// import "angular";

/**
 * Classe CursoAlunoService
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 */
class CursoAlunoService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @author Elcio Guimarães <elcioguimaraes@gmail.com>
   */
  constructor(CONFIG, $resource, $http, $q, LSService,$localStorage) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller
    this.$q = $q;

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'curso-aluno';
    this.storage = $localStorage;

    this.addCustomService('checkMatriculaAluno', CONFIG.urlApi + '/curso-aluno/verificar-matricula-aluno', 'GET', {});
  }

  /**
   * Verifica se a matricula logada esta vinculada ao usuario
   * @param matricula
   * @returns {Function}
     */
  verificaMatriculaAluno(idMatricula) {
    const defer = this.$q.defer();
    this.getResource().checkMatriculaAluno({id_matricula: idMatricula}, (response) => {
      this.setStorageCursoAtivo(response);
      defer.resolve(true);
    }, () => {
      defer.resolve(false);
    });
    return defer.promise;
  }

  /**
   * Seta a matricula no localStorage
   * @param curso
   * @param entidadeId
   */
  setStorageCursoAtivo(curso) {
    this.storage["matricula." + curso.id_matricula] =  curso;
  }

  /**
   * Retorna os dados da matricula que estão no localStorage
   * @param matriculaId
   * @param entidadeId
   */
  getStorageCursoAtivo(idMatricula) {
    const matricula = this.storage["matricula." + idMatricula];
    return matricula;
  }

  /**
   * Remove a matricula do localStorage
   * @param matriculaId
   * @param entidadeId
   */
  removeStorageCursoAtivo(idMatricula) {
    delete this.storage["matricula." + idMatricula];
  }

}

export default CursoAlunoService;
