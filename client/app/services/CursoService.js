import RestService from './RestService';

/**
 * Classe CursoService
 * @author Marcus Pereira <pereiramarcus93@gmail.com>
 */

class CursoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'curso';
    this.CONFIG = CONFIG;

    this.addCustomService('getDetalhesCoordenador', CONFIG.urlApi + '/curso/get-detalhes-coordenador', 'GET', {});
    this.addCustomService('getCoordenadorCurso', CONFIG.urlApi + '/curso/get-coordenador-curso', 'GET', {});
    this.addCustomService('getBoasVindasCurso', CONFIG.urlApi + '/curso/get-boas-vindas-curso', 'GET', {});
  }

  getCoordenadorCurso(idProjetoPedagogico) {
    const defer = this.q.defer();
    this.getResource()
      .getCoordenadorCurso({id_projetopedagogico: idProjetoPedagogico},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

  getDetalhesCoordenador(idUsuario, idEntidade) {
    const defer = this.q.defer();

    this.getResource()
      .getDetalhesCoordenador({id_usuario: idUsuario, id_entidade: idEntidade},
      (success) => {
        defer.resolve(success);
      },
      (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }

  getBoasVindasCurso(idProjetoPedagogico) {
    const defer = this.q.defer();

    this.getResource()
      .getBoasVindasCurso({id_projetopedagogico: idProjetoPedagogico},
      (success) => {
        defer.resolve(success);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }

}

export default CursoService;
