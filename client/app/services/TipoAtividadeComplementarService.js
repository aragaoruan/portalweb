import RestService from './RestService';

/**
 *
 * Classe para tratar os tipos de atividade complementar
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 *
 */
class TipoAtividadeComplementarService extends RestService {
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'tipo-atividade-complementar';
  }
}

export default TipoAtividadeComplementarService;
