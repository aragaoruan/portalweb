
const moment = require('moment');

/**
* Classe CursoAlunoService
* @author Elcio Guimarães <elcioguimaraes@gmail.com>
*/
class LSService  {

  /**
  * Metodo construtor
  * @param CONFIG
  * @param $resource
  * @param $http
  * @author Elcio Guimarães <elcioguimaraes@gmail.com>
  */
  constructor(CONFIG, md5, $window, $localStorage) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    this.storage =$localStorage;
    this.md5 = md5;
  }

  /**
  * Gera o ID para o cache
  * @param key
  * @param param
  * @returns {*}
  */
  getId(key, param){
    try {
      if (param)        {
        return key + '.' + this.md5.createHash(angular.toJson(param));
      }

      return key;
    }
    catch (err) {
      this.$log.error(err);
    }
  }

  /**
  * Limpa o cache
  * @param key
  * @returns {string}
  */
  clearStorage(key) {
    delete this.storage[key];
    delete this.storage[key + '.query'];
    delete this.storage[key + '.time'];
    delete this.storage[key + '.time.expire'];
    return 'Key ' + key + ' clear';
  }

  /**
  * Salva um valor no LocalStorage
  * Par controlarmos o cache, ao setar algum valor no LocalStorage criamos mais dois registros, o .time e o .time.expire
  * O .time salva o momento em que foi criado o registro e o .time.expire salva a quantidade de minutos que o mesmo deve durar
  *
  * @param key - ID
  * @param val - Valor
  * @param cacheForMinutes - Quantidade de minutos que o cache irá durar
  * @param param - Parâmetros para consulta à API, se informado é gerado um md5 e anexado ao ID para termos cache da consulta
  * @returns {LSFactory}
  */
  set(key, val, cacheForMinutes, param) {


    try {

      key = this.getId(key, param);

      cacheForMinutes || (cacheForMinutes = 5);

      if (angular.isObject(val)) {
        val = angular.toJson(val);
      }

      this.storage[key] =  val;

      const date = moment();
      this.storage[key + '.time'] = date.toISOString();
      this.storage[key + '.time.expire'] = cacheForMinutes;

      // console.log('Cache salvo: ', key, cacheForMinutes, param);

    }
    catch (err) {
      this.$log.error(err);
    }

  }

  /**
  * Retorna os dados do cache
  *
  * Neste momento o sistema verifica se temos os registros [key].time e [key].time.expire.
  * Tendo estas duas informações, fazemos a comparação da diferença em minutos do moneto em que foi salvo o cache com o momento atual
  * Caso essa diferença seja maior ou igual à quantidade de minutos informada no momento de criação do cache, o sistema apaga os dados do cache
  * retornando null, fazendo com que o sistema recrie o cache com os dados oriundos da API.
  *
  * Normalmente, quando salvamos dados da API no cache, salvamos uma string JSON, pis é o formato retornado pela nossa API.
  * Para melhorar a forma de tratar estes dados, o usuário pode informar a propriedade [convertToJson], ela irá dizer ao método
  * que o resultado deve ser um Objeto JSON e não uma string.
  *
  * @param key - ID
  * @param convertToJson - Se true, irá retornar o Objeto JSON referentes aos dados do cache
  * @param param - Parâmetros para consulta à API, se informado é gerado um md5 e anexado ao ID para retornarmos o cache da consulta
  * @returns {Storage}
  */
  get(key, convertToJson, param) {

    try {

      key = this.getId(key, param);

      const time = this.storage[key + '.time'];
      if (time) {
        let expire = this.storage[key + '.time.expire'];
        if (!expire) {
          expire = 5;
        }

        const date = moment();
        const cachedDate = moment(time);

        // console.log('Cache acessado: ', key, expire, date.diff(cachedDate, 'minutes'));

        if (expire && (expire <= date.diff(cachedDate, 'minutes'))) {

          // console.log('Apagando o Cache', expire, date.diff(cachedDate, 'minutes'));

          this.clearStorage(key);
          return;
        }

      }

      // console.log('Cache acessado: ', key);
      const cached = this.storage[key];

      // console.log('Consulta ao cache: ', key,  cached);

      if (cached && convertToJson)        {
        return angular.fromJson(cached);
      }

      return cached;
    }
    catch (err) {
      this.$log.error(err);
    }


  }


}

export default LSService;
