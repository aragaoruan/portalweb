import RestService from './RestService';

/**
 * Classe LinhaDeNegocioService
 * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
 */
class LinhaDeNegocioService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @author Arthur Luiz Lara QUites <arthur.quites@unyleya.com.br>
   */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'linha-de-negocio';

    this.CONFIG = CONFIG;

    this.addCustomService('getLinhaDeNegocio', CONFIG.urlApi + '/linha-de-negocio', 'GET', {});
  }

  /**
   * Pega a linha de negócio da entidade
   * @param id_entidade
   * @param funcao
   * @returns {Function|promise}
   * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
   */
  getLinhaDeNegocio(id_entidade) {


    const defer = this.q.defer();

    this.getResource()
      .getLinhaDeNegocio({id_entidade:id_entidade}, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }

}

export default LinhaDeNegocioService;
