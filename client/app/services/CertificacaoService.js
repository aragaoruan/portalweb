import RestService from "./RestService";

/***
 * Class CertificacaoService
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class CertificacaoService extends RestService {
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);

    this.$q = $q;
    this.api = 'curso-aluno';
    this.addCustomService('detalhesCertificacaoMatricula', CONFIG.urlApi + '/matricula-certificacao', 'GET', {});
  }

  /** @function detalhesCertificacaoMatricula
   * Retorna os dados na tb_matriculacertificacao baseando-se na matrícula. */

  detalhesCertificacaoMatricula(idMatricula){
    const defer = this.$q.defer();
    this.getResource().detalhesCertificacaoMatricula({id_matricula: idMatricula}, (success) => {
      defer.resolve(success);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

}

export default CertificacaoService;
