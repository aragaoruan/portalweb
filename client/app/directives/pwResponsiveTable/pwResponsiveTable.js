/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import componente do modulo
import pwResponsiveTableComponent from './pwResponsiveTable.component';
import pwResponsiveTableDynamicComponent from './pwResponsiveTableDynamic.component';

//Import do modulo de services do portal
import Services from '../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pwResponsiveTableModule = angular.module('pwResponsiveTable', [
  Services.name //Injecao modulo services do portal
])

.directive('pwResponsiveTable', pwResponsiveTableComponent).directive('td', [pwResponsiveTableDynamicComponent]);

export default pwResponsiveTableModule;
