import controller from './dtbot.controller';
import template from './dtbot.html';

const dtBotComponent = {
  name: 'dtbot',
  restrict: 'E',
  bindings: {
    bot: '@',
    matricula: '@',
    permitido:'@',
    url:'@'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default dtBotComponent;
