import imagem_bot from '../../../assets/img/bot_unyleya.png';

class DtBotController {
  constructor($window, $scope, $document) {
    'ngInject';

    this.imagem_bot = imagem_bot;
    this.$window = $window;
    this.$scope = $scope;
    this.$document = $document;
    
    $scope.$watch(() => {
      return this.url;
    }, (valor) => {
      if (angular.isDefined(valor) && valor !== ''){
        this.loaded = 1;

        var scriptElem = angular.element('<script/>');
        scriptElem.attr("src", this.url);
        scriptElem.attr("id", "dtbot-script");

        var body = $document.find('body');
        
        body.append(scriptElem);
        this.iniciar();
      }
    });
  }

  iniciar(){
    var bot = this.$document.find('dtbot-iframe');
    this.$scope.$watch(() => {
      return this.$window.DTBOT;
    },
    (valor) => {
      if (angular.isDefined(valor) && angular.isFunction (valor.start) && bot){
        this.$scope.$watch(
          () => {
            return this.$window.BOTFLOATER.element;
          },
          (valor) => {
            if (angular.isDefined(valor)){
              valor.addEventListener('click', () => {
                this.$window.DTBOT.start(this.bot, true);
              });
            }
          }
        );
      }
    });
  }

}

export default DtBotController;
