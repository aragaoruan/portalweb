import circle from "../../../assets/img/circulo_grafico.svg";
class PwChartController {
  constructor() {
    this.name = 'pwChart';
    this.graphCircle = circle;

    if (!this.percent) {
      this.percent = 0;
    }


    //Configura chart na primeira execução da Controller.
    this.configurarChart();

    //Atualiza dados do chart nas demais execuções.
    // this.$watchGroup(['percent', 'statuscomplete', 'percent'], function () {
    //   configurarChart();
    // });
  }

  configurarChart() {
    this.strokeColor = '';
    this.salaNaoIniciada = this.statussala === 2;
    if (this.mysize === 'small'){
      this.size = 60;
    }
    else {
      this.size = 85;
    }

    // Calculando a área transparente do gráfico.
    let result = 100 - this.percent;

    // Definindo cores com base no progresso da sala.
    switch (this.statuscomplete) {
    case "Adiantado":
      this.strokeColor = '#50BCBD';
      break;
    case "Em dia":
      this.strokeColor = '#50BCBD';
      break;
    case "Completo":
      this.strokeColor = '#7FA2D5';
      this.salaCompleta = true;
      this.percent = 100;
      result = 0;
      break;
    case "Indefinido":
      this.strokeColor = '#50BCBD';
      break;
    case "Atrasado":
      this.strokeColor = '#E84242';
      break;
    case "Futuro":
      this.hideNumber = true;
      result = 100;
      this.strokeColor = '#FFFFFF';
      break;
    default:
      this.strokeColor = '#FFFFFF';
      break;
    }

    /* Aplicando cores definidas acima no chart.
     Remove o # da string da classe, para ser utilizada corretamente
     na classe do template da diretiva.*/

    this.colorClass = this.strokeColor.substr(1);
    this.labels = ["", ""]; // Deixando labels vazias.
    this.data = [this.percent, result]; // Definindo dados do gráfico
    this.options = {
      cutoutPercentage: 66,
      tooltips: {
        enabled: false
      }
    };

    if (this.animation === 'false')      {
      this.options.animation = false;
    }


    // Aplicando ao gráfico a cor selecionada baseada no status.

    this.colors = [
      {
        backgroundColor: this.strokeColor,
        borderColor: this.strokeColor,
        pointBackgroundColor: this.strokeColor,
        pointBorderColor: this.strokeColor,
        pointHoverBackgroundColor: this.strokeColor,
        pointHoverBorderColor: this.strokeColor
      },
      {
        backgroundColor: "rgba(0,0,0,0)",
        borderColor: "rgba(0,0,0,0)",
        pointBackgroundColor: "rgba(0,0,0,0)",
        pointBorderColor: "rgba(0,0,0,0)",
        pointHoverBackgroundColor: "rgba(0,0,0,0)",
        pointHoverBorderColor: "rgba(0,0,0,0)"
      }
    ];
  }

}

export default PwChartController;
