import template from './pwChart.html';
import controller from './pwChart.controller';
import './pwChart.scss';

const pwChartComponent = {
  restrict: 'E',
  bindings: {
    mysize: '@',
    percent: '=percent',
    color: '@',
    statuscomplete: '=statuscomplete',
    statussala: '=statussala',
    animation: '@'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pwChartComponent;
