export default angular.module('app.portalconstante', [])

  //constants da tb_tipoendereco
  .constant('TipoEnderecoConstante', {
    CORRESPONDENCIA: 5,
    RESIDENCIAL: 1,
    COMERCIAL: 2,
    MATRIZ: 3,
    FILIAL: 4,
    APLICADOR_DE_PROVA: 6
  })
  .constant('MeioPagamentoConstante', {
    BOLETO: 2
  })
  .constant('SituacaoConstante', {
    AGENDAMENTO_AGENDADO: 68,
    AGENDAMENTO_REAGENDADO: 69,
    AGENDAMENTO_CANCELADO: 70,
    AGENDAMENTO_LIBERADO: 129,
    AGENDAMENTO_ABONADO: 130
  })
  .constant('StatusLancamentoConstante', {
    PAGO: 1,
    APAGAR: 2,
    ATRASADO: 3
  })
  .constant('MensagemPadrao', {
    ARQUIVO_FORA_DO_FORMATO_PERMITIDO: ', você só pode fazer upload de arquivos nos formatos JPEG, PNG, PDF, DOC, DOCX, XLS e XLSX.',
    ARQUIVO_TAMANHO: ', você só pode fazer upload de arquivos com tamanho inferior a 10MB.',
    OCORRENCIA_CADASTRADA_SUCESSO: ', sua ocorrência foi cadastrada com sucesso.',
    INTERACAO_CADASTRADA_SUCESSO: ', sua interação foi cadastrada com sucesso.5',
    OCORRENCIA_ENCERRADA_SUCESSO: ', sua ocorrência foi encerrada com sucesso.',
    REABRIR_OCORRENCIA_SUCESSO: ', sua ocorrência foi reaberta com sucesso.',
    ACEITE_CONTRATO_DISCIPLINAS: ', você precisa aceitar o contrato para ter acesso às disciplinas.',
    USUARIO_INCORRETO: 'Usuário, e-mail ou senha incorretos. Por favor, verifique as informações e preencha os campos novamente.',
    SENHA_ALTERADA_SUCESSO: ', sua senha foi alterada com sucesso!',
    RECUPERA_SENHA_SUCESSO: 'Um e-mail com instruções para a recuperação da sua senha foi enviado com sucesso.',
    ERRO_REQUISICAO: 'Houve algum problema na requisição, favor tentar mais tarde!',
    DADOS_CADASTRAIS_SUCESSO: 'Dados alterados com sucesso.'
  })

  //Constantes de evolução para tb_matricula.
  .constant('EvolucaoMatriculaConstante', {
    AGUARDANDO_CONFIRMACAO: 5,
    CURSANDO: 6,
    CERTIFICADO: 16,
    CONCLUINTE: 15,
    TRANSFERIDO: 20,
    TRANCADO: 21,
    SEM_RENOVACAO: 25,
    RETORNADO: 26,
    CANCELADO: 27,
    BLOQUEADO: 40,
    ANULADO: 41,
    TRANSFERIDO_DE_ENTIDADE: 47,
    DECURSO_DE_PRAZO: 69
  })

  .constant('PaisConstante', {
    BRASIL: 22
  })

  .constant('MensagensInfoEvolucaoConstante', {
    PERFIL_DESATIVADO: "Seu perfil não está mais ativo no Portal do Aluno.",
    BLOQUEADO: "Seu acesso ao curso está temporariamente suspenso. Para saber mais, acesse o Serviço de Atenção.",
    TRANCADO: "Seu curso está temporariamente trancado. Para retornar seus estudos acesse o Serviço de Atenção.",
    CERTIFICADO_APOS_30_DIAS: "Seu perfil não está mais ativo no Portal do Aluno porque você já concluiu seu curso há" +
    " mais de 30 dias e seu certificado já foi emitido. Parabéns pela sua conquista e aproveite para consultar as " +
    "condições especiais para fazer outro curso."
  })

  .constant('SituacaoTCCConstante', {
    PENDENTE: 203,
    EM_ANALISE: 204,
    DEVOLVIDO_AO_ALUNO: 205,
    AVALIADO_PELO_ORIENTADOR: 206,
    DEVOLVIDO_AO_TUTOR: 207
  })

  .constant('TbSistema', {
    PortalAluno: 4
  });
