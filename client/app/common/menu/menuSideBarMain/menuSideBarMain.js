//import angular from 'angular';
import uiRouter from 'angular-ui-router';
import menuSideBarMainComponent from './menuSideBarMain.component';
import Services from '../../../services/Services';

const menuSideBarMainModule = angular.module('menuSideBar', [
  uiRouter,
  Services.name
]).config(($stateProvider, $urlRouterProvider) => {
  "ngInject";

  $urlRouterProvider.otherwise('/');

  $stateProvider.state('app.menusidebar', {
    url: '',
    views: {
      content: {
        template: '<menu-side-bar></menu-side-bar>'
      }
    }
  });

}).component('menuSideBar', menuSideBarMainComponent);

export default menuSideBarMainModule;
