import loadingGif from '../../../../assets/img/loading.gif';
import googlePlay from '../../../../assets/img/google_play.svg';
import appleStore from '../../../../assets/img/apple_store.svg';
import cellphone from '../../../../assets/img/cellphone.svg';

class menuSideBarListController {
  constructor($state, $rootScope, NotifyService, MenuService, CursoAlunoService, AuthService, $window) {
    'ngInject';

    this.name = 'menuSideBarList';
    this.loadingGif = loadingGif;
    this.cellphone = cellphone;
    this.googlePlay = googlePlay;
    this.appleStore = appleStore;
    this.loading = false;
    this.$state = $state;
    this.$scope = $rootScope;
    this.$window = $window;

    this.toaster = NotifyService;
    this.CursoAlunoService = CursoAlunoService;
    this.MenuService = MenuService;
    this.AuthService = AuthService;
    this.itemover = false;

    this.cursoAtivo = {};
    this.menusidebar = [];
    this.idMatricula = this.$state.params.idMatriculaAtiva;
    this.getDadosCursoAtivo();
    this.buscaMenuSideBar();

    this.showTrocarCurso = false;

    this.CursoAlunoService.getResource().getAll({}, (response) => {
      if (response.length && response[0].cursos.length > 1) {
        this.showTrocarCurso = true;
      }
    });

  }

  /**
   * Função que busca dos dados do usuário
   * @param idUsuario
   * @param idEntidade
   */
  buscaMenuSideBar = () => {
    this.MenuService.getMenuSideBar(this.cursoAtivo.id_entidade).then((response) => {
      this.menusidebar = response;
      this.exibirBibliotecaTemporario();
    }, (error) => {
      this.toaster.notify(error.data);
    });
  }

  /**
   * Esta função é responsável por exibir o html que foi colocado no menu para exibir um novo item de menu que redireciona
   * o usuário do id que esta na função para o link de uma nova biblioteca de livros. Esse item de menu foi colocado
   * assim porque foi decidido entre os gestores que iriamos fazer isso para uma apresentação do portal do aluno para
   * uma equipe do MEC, então em um momento oportuno isso será removido do código junto com seu respectivo html.
   * @returns {boolean}
   */
  exibirBibliotecaTemporario = () => {
    const idUserTemporario = 10166204;

    if (this.cursoAtivo.id_usuario === idUserTemporario) {
      return true;
    }

    return false;

  }

  /**
   * @deprecated
   * @param rota
   */
  redirecionar(rota) {
    const idMatricula = this.$state.params.idMatriculaAtiva;
    this.$state.go(rota, {
      idMatriculaAtiva: idMatricula
    });
  }

  /**
   * Recupera os dados da matricula ativa
   */
  getDadosCursoAtivo() {
    //recupera o parametro da matricula ativa
    const idMatricula = this.$state.params.idMatriculaAtiva;
    this.cursoAtivo.id_matricula = idMatricula;
    this.cursoAtivo.curso = this.CursoAlunoService.getStorageCursoAtivo(idMatricula);
    this.cursoAtivo.id_entidade = this.AuthService.getUserCredential('user_entidade');
    this.cursoAtivo.id_usuario = this.AuthService.getUserCredential('user_id');

  }

  onToggle() {
    this.$scope.$root.wrapper.onToggle();
  }

  openGooglePlay() {
    this.$window.open("https://play.google.com/store/apps/details?id=com.unyleya.portalapp&hl=pt_BR");
  }

  openAppleStore() {
    this.$window.open("https://itunes.apple.com/br/app/portal-do-aluno/id1037099343?mt=8");
  }
}

export default menuSideBarListController;
