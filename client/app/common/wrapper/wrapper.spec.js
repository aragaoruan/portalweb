import WrapperModule from './wrapper';
import WrapperController from './wrapper.controller';
import WrapperComponent from './wrapper.component';
import WrapperTemplate from './wrapper.html';

describe('Wrapper', () => {
  let $rootScope, makeController;

  beforeEach(window.module(WrapperModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new WrapperController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = WrapperComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(WrapperTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(WrapperController);
    });
  });
});
