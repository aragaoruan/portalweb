import template from './wrapper.html';
import controller from './wrapper.controller';
import './wrapper.scss';

const wrapperComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'wrapper'
};

export default wrapperComponent;
