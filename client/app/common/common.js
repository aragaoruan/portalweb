import angular from 'angular';
import Wrapper from './wrapper/wrapper';
import PerfectScrollbar from "./perfectScrollbar/perfectScrollbar";

//Import menuSideBarMain
import MenuSideBarMain from './menu/menuSideBarMain/menuSideBarMain';

//Import menuSideBarList
import MenuSideBarList from './menu/menuSideBarList/menuSideBarList';

export default angular.module("app.common", [
  Wrapper.name,
  MenuSideBarMain.name,
  MenuSideBarList.name,
  PerfectScrollbar.name
]);
