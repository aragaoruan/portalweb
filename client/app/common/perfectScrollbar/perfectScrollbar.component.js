import template from './perfectScrollbar.html';
import controller from './perfectScrollbar.controller';
import './perfectScrollbar.scss';

const perfectScrollbarComponent = {
  transclude: true,
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default perfectScrollbarComponent;
