module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 8,
    sourceType: 'module',
    ecmaFeatures: {
      experimentalObjectRestSpread: true
    }
  },
  env: {
    browser: true,
    node: true
  },
  extends: [
    'eslint:recommended',
    'angular'
  ],
  plugins: [
    'html',
    'angular',
  ],
  'rules': {
    'global-require': 0,
    'no-shadow': 0,
    "strict": 0,
    'no-unused-vars':2,
    'angular/no-service-method' : 0,
    'angular/log' : 0,
    'angular/timeout-service':0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'no-console': process.env.NODE_ENV === 'production' ? 2 : 0,
//tips
    "brace-style": [2, "stroustrup", {"allowSingleLine": false}],
    "camelcase": 0,
    "comma-dangle": [2, "never"],
    "curly": 2,
    "dot-notation": 2,
    "eqeqeq": 2,
    "indent": [1, 2],
    "lines-around-comment": [2, {"allowBlockStart": true, "beforeBlockComment": true, "beforeLineComment": true}],
    "new-parens": 2,
    "no-bitwise": 2,
    "no-cond-assign": 2,
    "no-dupe-args": 2,
    "no-dupe-keys": 2,
    "no-empty": 2,
    "no-invalid-regexp": 2,
    "no-mixed-spaces-and-tabs": [2, "smart-tabs"],
    "no-multiple-empty-lines": [2, {"max": 2}],
    "no-undef": 2,
    "no-underscore-dangle": 2,
    "no-unreachable": 2,
    "one-var": [2, "never"],
    "quote-props": [2, "as-needed"],
    "semi": [2, "always"],
    "keyword-spacing": 2,
    "space-unary-ops": [2, {"words": true, "nonwords": false}],
    "wrap-iife": [2, "outside"],
    "yoda": [2, "never"],

    // ES6 Stuff
    "arrow-parens": 2,
    "arrow-spacing": 2,
    "constructor-super": 2,
    "no-class-assign": 2,
    "no-const-assign": 2,
    "no-dupe-class-members": 2,
    "no-this-before-super": 2,
    "object-shorthand": 0,
    "prefer-arrow-callback": 2,
    "prefer-const": 2
  }
}

// 'import/no-unresolved': 0,
// 'no-param-reassign': 0,
